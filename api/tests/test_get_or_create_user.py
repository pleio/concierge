import json
import uuid

from django.urls import reverse
from mixer.backend.django import mixer
from oidc_provider.models import Client

from api.serializers import UserSerializer
from core.models import OriginSite, User, UserSiteRegistration
from core.tests import TestCase


class TestGetOrCreateUserTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.oidc_client = Client.objects.create(client_id="123456", client_secret=str(uuid.uuid4()))
        self.origin_site = mixer.blend(OriginSite)
        self.AUTHENTICATION = {
            "HTTP_X_OIDC_CLIENT_ID": self.oidc_client.client_id,
            "HTTP_X_OIDC_CLIENT_SECRET": self.oidc_client.client_secret,
        }
        self.EMAIL = "foo@bar.baz"
        self.NAME = "Mister Foo"
        self.GET_OR_CREATE_USER = reverse("get_or_create_user")

    def test_without_name_given(self):
        # Given.
        User.objects.filter(email=self.EMAIL).delete()

        # When.
        response = self.client.post(
            self.GET_OR_CREATE_USER,
            data={
                "email": self.EMAIL,
                "origin_site_url": self.origin_site.origin_site_url,
                "actor_name": "Foo",
            },
            content_type="application/json",
            **self.AUTHENTICATION,
        )

        # Then.
        self.assertEqual(200, response.status_code)
        response_data = json.loads(response.getvalue().decode())
        self.assertTrue(User.objects.filter(email=self.EMAIL).exists())
        self.assertTrue(
            UserSiteRegistration.objects.filter(user__email=self.EMAIL, origin_site=self.origin_site).exists()
        )
        self.assertEqual(
            response_data,
            {
                **UserSerializer(User.objects.get(email=self.EMAIL)).data,
                "_is_new": True,
            },
        )

    def test_with_name_given(self):
        # When.
        response = self.client.post(
            self.GET_OR_CREATE_USER,
            data={
                "email": self.EMAIL,
                "name": self.NAME,
                "actor_name": "Foo",
                "origin_site_url": self.origin_site.origin_site_url,
            },
            content_type="application/json",
            **self.AUTHENTICATION,
        )

        # Then.
        self.assertEqual(200, response.status_code)
        response_data = json.loads(response.getvalue().decode())
        self.assertEqual(response_data["name"], self.NAME)
        self.assertTrue(
            UserSiteRegistration.objects.filter(user__email=self.EMAIL, origin_site=self.origin_site).exists()
        )

    def test_invalid_request_type(self):
        # When.
        response = self.client.put(
            self.GET_OR_CREATE_USER, data={"email": self.EMAIL}, content_type="application/json", **self.AUTHENTICATION
        )

        # Then.
        self.assertEqual(response.status_code, 400)

    def test_invalid_request_content(self):
        # When.
        response = self.client.put(
            self.GET_OR_CREATE_USER, data="Not json", content_type="application/json", **self.AUTHENTICATION
        )

        # Then.
        self.assertEqual(response.status_code, 400)

    def test_invalid_email_address(self):
        # When.
        response = self.client.put(
            self.GET_OR_CREATE_USER,
            data={"email": "not-a-mail"},
            content_type="application/json",
            **self.AUTHENTICATION,
        )

        # Then.
        self.assertEqual(response.status_code, 400)

    def test_without_origin_site(self):
        # When.
        response = self.client.post(
            self.GET_OR_CREATE_USER,
            data={"email": self.EMAIL, "actor_name": "Foo"},
            content_type="application/json",
            **self.AUTHENTICATION,
        )

        # Then.
        self.assertEqual(200, response.status_code)
        self.assertTrue(User.objects.filter(email=self.EMAIL).exists())
        self.assertFalse(
            UserSiteRegistration.objects.filter(user__email=self.EMAIL, origin_site=self.origin_site).exists()
        )

    def test_with_existing_user(self):
        # Given.
        user = mixer.blend(User, email=self.EMAIL)

        # When.
        response = self.client.post(
            self.GET_OR_CREATE_USER,
            data={"email": self.EMAIL, "actor_name": "Foo"},
            content_type="application/json",
            **self.AUTHENTICATION,
        )

        # Then.
        response_data = json.loads(response.getvalue().decode())
        self.assertEqual(response_data["guid"], user.id)

    def test_unauthorized_request(self):
        # When.
        response = self.client.post(self.GET_OR_CREATE_USER, data={}, content_type="application/json")

        # Then.
        self.assertEqual(403, response.status_code)
