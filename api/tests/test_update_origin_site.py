import uuid
from unittest import mock

from oidc_provider.models import Client

from core.models import OriginSite
from core.tests import TestCase


class TestUpdateOriginSiteTestCase(TestCase):
    API_URL = "/api/users/update_origin_site"

    def setUp(self):
        super().setUp()

        self.oidc_client = Client.objects.create(client_id="123456", client_secret=str(uuid.uuid4()))
        self.SITE_URL = "https://demo.net"
        self.SITE_NAME = "Just a test"
        self.SITE_DESCRIPTION = "How would you describe this demo?"
        self.SITE_FAVICON = "https://demo.net/favicon.png"

    def tearDown(self):
        OriginSite.objects.all().delete()
        super().tearDown()

    @mock.patch("django.views.generic.base.logger.warning")
    def test_update_requires_complete_data(self, mocked_warning):
        response = self.client.post(
            self.API_URL,
            HTTP_X_OIDC_CLIENT_ID=self.oidc_client.client_id,
            HTTP_X_OIDC_CLIENT_SECRET=self.oidc_client.client_secret,
        )
        self.assertEqual(response.status_code, 400)

    @mock.patch("core.tasks.refresh_site_favicon.delay")
    def test_update_produces_new_instance(self, mocked_refresh_site_icon):
        response = self.client.post(
            self.API_URL,
            data={
                "origin_site_url": self.SITE_URL,
                "origin_site_name": self.SITE_NAME,
                "origin_site_description": self.SITE_DESCRIPTION,
            },
            HTTP_X_OIDC_CLIENT_ID=self.oidc_client.client_id,
            HTTP_X_OIDC_CLIENT_SECRET=self.oidc_client.client_secret,
        )
        # OK signal?
        self.assertEqual(response.status_code, 200)

        # created origin site?
        origin_site = OriginSite.objects.get(origin_site_url=self.SITE_URL)
        self.assertEqual(origin_site.origin_site_name, self.SITE_NAME)
        self.assertEqual(origin_site.origin_site_description, self.SITE_DESCRIPTION)

        # Scheduled refresh_site_icon?
        self.assertTrue(mocked_refresh_site_icon.called)
