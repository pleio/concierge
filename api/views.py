import json
import logging
import os
from mimetypes import guess_type
from wsgiref.util import FileWrapper

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import (
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
    JsonResponse,
    StreamingHttpResponse,
)
from django.utils import timezone
from django.utils.html import conditional_escape
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.permissions import (
    NoAccessError,
    assert_pleio_subsite_access,
    require_valid_pleio_origin,
)
from api.serializers import AccountDeletedSerializer, UserSerializer
from core.avatar_processing import Avatar
from core.lib import build_url, mimetype_from_filename
from core.login_session_helpers import is_name_valid
from core.mailers import send_welcome_invited_user
from core.models import EventLog, OriginSite, PendingUser, User, UserSiteRegistration

logger = logging.getLogger(__name__)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def me(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def change_avatar(request):
    avatar = request.FILES.get("avatar")

    try:
        if avatar:
            mimetype = mimetype_from_filename(avatar["name"])
            assert mimetype.startswith("image/"), "invalid_avatar"

            request.user.avatar = avatar
            request.user.save()

        return Response({"success": True})
    except AssertionError as e:
        return Response({"success": False, "message": str(e)})


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def remove_avatar(request):
    request.user.avatar = None
    request.user.save()

    return Response({"success": True})


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def change_name(request):
    name = request.POST.get("name")
    user = request.user

    if name:
        if is_name_valid(name):
            user.name = name
            user.save()
        else:
            return Response({"success": False, "message": "invalid_name"})

    return Response({"success": True, "message": ""})


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def change_email(request):
    new_email = request.POST.get("email")

    try:
        user = User.objects.get(email__iexact=new_email)
    except User.DoesNotExist:
        user = None

    if user:
        return Response({"success": False, "message": "email_already_in_use"})

    try:
        validate_email(new_email)
    except ValidationError:
        return Response({"success": False, "message": "invalid_email"})

    request.user.new_email = new_email
    request.user.send_change_email_activation_token()
    request.user.save()

    EventLog.add_event(request, EventLog.EMAIL_CHANGE_PENDING)

    return Response({"success": True, "message": ""})


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def change_password(request):
    old_password = request.POST.get("old_password")
    new_password = request.POST.get("new_password")

    user = authenticate(username=request.user.email, password=old_password)

    if not user:
        return Response({"success": False, "message": "invalid_old_password"})

    try:
        validate_password(new_password, request.user)
    except ValidationError:
        return Response({"success": False, "message": "invalid_new_password"})

    user.set_password(new_password)
    user.save()

    return Response({"success": True, "message": ""})


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def deleted_accounts(request):
    allowed_filters = ["event_time", "event_time__gt", "event_time__lt"]
    limit = 500

    resultset = EventLog.objects.filter(event_type=EventLog.ACCOUNT_DELETED).order_by("event_time")

    for allowed_filter in allowed_filters:
        if request.GET.get(allowed_filter):
            resultset = resultset.filter(**{allowed_filter: request.GET.get(allowed_filter)})

    serializer = AccountDeletedSerializer(resultset[:limit], many=True)
    return Response(serializer.data)


@require_valid_pleio_origin
def fetch_profile(request, user_id):
    try:
        user = User.objects.get(id=user_id)
        return JsonResponse(UserSerializer(user).data)
    except (User.DoesNotExist, AssertionError, ValidationError):
        pass

    return JsonResponse({"status": "Not found."}, status=404)


@require_valid_pleio_origin
def fetch_profile_mail(request, email):
    try:
        user = User.objects.get(email__iexact=email)
        return JsonResponse(UserSerializer(user).data)
    except (User.DoesNotExist, AssertionError, ValidationError):
        pass

    return JsonResponse({"status": "Not found."}, status=404)


@require_valid_pleio_origin
def fetch_avatar(request, email):
    try:
        profile: User = User.objects.get(email__iexact=email)
        return JsonResponse(
            {
                "avatarUrl": profile.avatar_url,
                "originalAvatarUrl": profile.original_avatar_url,
            }
        )
    except Exception:
        pass

    return JsonResponse({"avatarUrl": User.default_avatar_url(), "originalAvatarUrl": None})


@csrf_exempt
@require_valid_pleio_origin
def get_or_create_user(request):
    try:
        content = json.loads(request.body.decode())
        assert request.method == "POST", "Should be used as a http-POST request"
        assert "email" in content, "Should have email parameter"
        assert "actor_name" in content, "Should have an actor"
        assert "@" in content["email"], "Should have a valid email parameter"

        user = User.objects.filter(email__iexact=content["email"]).first()
        created = False
        if not user:
            created = True
            email = content["email"]
            name = content.get("name") or slugify(email[: email.index("@")])
            user = User.objects.create(email=email, name=name, is_active=True)

        site = None
        if "origin_site_url" in content:
            if site := OriginSite.objects.filter(origin_site_url=content["origin_site_url"]).first():
                UserSiteRegistration.objects.get_or_create(
                    user=user,
                    origin_site=site,
                )
        if created and (next := content.get("next")):
            if site:
                url = build_url(content.get("origin_site_url"), "login", next=next)
            elif "origin_site_url" in content:
                url = build_url(content.get("origin_site_url"), next=next)
            else:
                url = next
            pending_user = PendingUser.objects.create(user=user, type=PendingUser.PENDING_NEW_PROFILE, next=url)
            send_welcome_invited_user(pending_user, site, content["actor_name"])

        return JsonResponse(
            {
                **UserSerializer(user).data,
                "_is_new": created,
            }
        )
    except json.JSONDecodeError:
        return JsonResponse({"msg": "Invalid input format"}, status=400)
    except AssertionError as e:
        return JsonResponse({"msg": conditional_escape(str(e))}, status=400)
    except Exception as e:
        return JsonResponse(
            {"msg": conditional_escape(str(e)), "type": e.__class__},
            status=400,
        )


@csrf_exempt
@require_valid_pleio_origin
def register_origin_site(request, user_id):
    try:
        assert request.method == "POST", "Invalid request"
        assert "origin_site_url" in request.POST, "Provide site url"
        assert "origin_site_name" in request.POST, "Provide site name"

        user = User.objects.get(id=user_id)

        origin_site, created = OriginSite.objects.get_or_create(origin_site_url=request.POST.get("origin_site_url"))
        if created:
            from core.tasks import refresh_site_favicon

            refresh_site_favicon.delay(origin_site.id)
        if "origin_site_name" in request.POST:
            origin_site.origin_site_name = request.POST.get("origin_site_name")
        if "origin_site_description" in request.POST:
            origin_site.origin_site_description = request.POST.get("origin_site_description")
        if "origin_site_api_token" in request.POST:
            origin_site.origin_site_api_token = request.POST.get("origin_site_api_token")
        origin_site.save()

        registration, _ = UserSiteRegistration.objects.get_or_create(user=user, origin_site=origin_site)

        if "origin_token" in request.POST:
            registration.origin_token = request.POST.get("origin_token")
        if "registration_date" in request.POST:
            registration.created_at = request.POST.get("registration_date")
        registration.save()

        return JsonResponse({"msg": "OK"})
    except AssertionError as e:
        return HttpResponseBadRequest(reason=conditional_escape(e))
    except User.DoesNotExist:
        return HttpResponseNotFound()


@csrf_exempt
@require_valid_pleio_origin
def update_origin_site(request):
    try:
        assert request.method == "POST", "Invalid request"
        assert request.POST.get("origin_site_url"), "Provide site url"

        origin_site, _ = OriginSite.objects.get_or_create(origin_site_url=request.POST.get("origin_site_url"))
        if "origin_site_name" in request.POST:
            origin_site.origin_site_name = request.POST.get("origin_site_name")
        if "origin_site_description" in request.POST:
            origin_site.origin_site_description = request.POST.get("origin_site_description")
        if "origin_site_api_token" in request.POST:
            origin_site.origin_site_api_token = request.POST.get("origin_site_api_token")
        origin_site.save()

        from core.tasks import refresh_site_favicon

        refresh_site_favicon.delay(origin_site.id)

        return JsonResponse({"msg": "OK"})
    except AssertionError as e:
        return HttpResponseBadRequest(reason=conditional_escape(e))


def assert_user_match(request, user_id):
    if request.user.is_anonymous or str(request.user.id) != str(user_id):
        raise NoAccessError()


def avatar(request, user_id, filename):
    try:
        if request.user.is_anonymous:
            assert_pleio_subsite_access(request)
        else:
            assert_user_match(request, user_id)

        size_input = request.GET.get("size", "large")
        if size_input in Avatar.SIZES:
            size = Avatar.SIZES[size_input]
        else:
            size = int(size_input)

        assert size in settings.ALLOWED_AVATAR_SIZES, "Size not allowed"
        user = User.objects.get(id=user_id, is_active=True)

        assert bool(user.avatar.name), "User has no avatar"
        assert os.path.exists(user.avatar.path), "User avatar file not found"

        avatar = Avatar(user=user)
        avatar_path = avatar.thumbnail_size_path(size)

        mime_type, _ = guess_type(avatar_path)
        assert mime_type.startswith("image/"), "Not an image"

        response = StreamingHttpResponse(
            streaming_content=FileWrapper(open(avatar_path, "rb")),
            content_type=mime_type,
        )
        response["Content-Length"] = os.path.getsize(avatar_path)
        response["Cache-Control"] = "public, max-age: %s" % 31536000
        response["Expires"] = timezone.localtime() + timezone.timedelta(days=365)
        return response

    except (ValueError, AssertionError, User.DoesNotExist, FileNotFoundError):
        return HttpResponseNotFound()
    except NoAccessError:
        return HttpResponseForbidden()
