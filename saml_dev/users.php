<?php

$config = array(
    'admin' => array(
        'core:AdminPassword',
    ),

    'example-userpass' => array(
        'exampleauth:UserPass',
        'user1:password' => array(
            'id' => 'f2d75402-e1ae-40fe-8cc9-98ca1ab9cd5e',
            'emailaddress' => 'user1@pleio.nl'
        ),
        'user2:password' => array(
            'id' => 'f2a94916-2fcb-4b68-9eb1-5436309006a3',
            'emailaddress' => 'user2@pleio.nl'
        ),
    ),
);
