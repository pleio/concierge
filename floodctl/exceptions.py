from django.contrib import messages
from django.utils.translation import gettext as _


class FloodOverflowError(Exception):
    """Thrown if the current login attempt is suspicious."""

    def report(self, request):
        messages.error(request, _("Error message due to abuse protection. Please try again later."))
