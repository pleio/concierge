import logging

from django.conf import settings
from django.db import models
from django.utils import timezone

from floodctl.exceptions import FloodOverflowError

logger = logging.getLogger(__name__)


def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip


def flood_token(request, user):
    return "%s|%s" % (get_client_ip(request), user.id)


class FloodLogManager(models.Manager):
    def cleanup_expired_records(self):
        overflow = self.get_queryset().filter(expires__lt=timezone.now() - timezone.timedelta(days=60))
        overflow.delete()

    def reset_target(self, request, target):
        overflow = self.get_queryset().filter(target=target, ip=get_client_ip(request))
        overflow.delete()

    def assert_not_blocked(self, request, target):
        records = self.get_queryset().filter(target=target, ip=get_client_ip(request), expires__gte=timezone.now())
        if records.count() >= settings.FLOOD_THRESHOLD:
            msg = "Flood overflow"
            raise FloodOverflowError(msg)

        self.assert_ip_not_blocked(request)

    def assert_ip_not_blocked(self, request):
        ip = get_client_ip(request)
        records = self.get_queryset().filter(ip=ip, expires__gte=timezone.now())
        if records.count() >= settings.FLOOD_IP_THRESHOLD:
            msg = "IP Flood overflow"
            raise FloodOverflowError(msg)

    def add_record(self, request, target):
        self.create(
            target=target,
            ip=get_client_ip(request),
            expires=timezone.now() + timezone.timedelta(minutes=settings.FLOOD_EXPIRE),
        )


class FloodLog(models.Model):
    objects = FloodLogManager()

    ip = models.GenericIPAddressField()
    target = models.CharField(max_length=128)
    expires = models.DateTimeField()

    def __str__(self):
        return "%s@%s" % (self.target, self.ip)
