from django.apps import AppConfig


class FloodCtlConfig(AppConfig):
    name = "floodctl"
