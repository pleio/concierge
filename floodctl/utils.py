from contextlib import contextmanager

from django.http import Http404
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _

from floodctl.exceptions import FloodOverflowError
from floodctl.models import FloodLog


@contextmanager
def flood_protection(request, target=None, e=None):
    try:
        if target:
            FloodLog.objects.assert_not_blocked(request, target)
        else:
            FloodLog.objects.assert_ip_not_blocked(request)
        yield
    except FloodOverflowError:
        if e:
            raise e
        raise


class RedirectOnOverflowMixin:
    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, *args, **kwargs)
        except FloodOverflowError as e:
            e.report(request)
            return redirect("/")


class FloodProtectionMixin(RedirectOnOverflowMixin):
    flood_overflow_message = _("Too many failed attempts.")

    def dispatch(self, request, *args, **kwargs):
        try:
            with flood_protection(request, e=Http404(self.flood_overflow_message)):
                return super().dispatch(request, *args, **kwargs)
        except FloodOverflowError as e:
            e.report(request)
            return redirect("/")
