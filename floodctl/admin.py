from django.contrib import admin

from floodctl.models import FloodLog


class FloodLogAdmin(admin.ModelAdmin):
    search_fields = ("ip", "target")
    list_display = ("__str__", "expires")


admin.site.register(FloodLog, FloodLogAdmin)
