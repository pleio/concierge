import csv
from io import StringIO
from urllib.parse import urljoin

from celery import shared_task
from django.apps import apps
from django.conf import settings
from django.core.mail import EmailMultiAlternatives


@shared_task
def create_report(report_guid):
    from reporting.models import Report

    report = Report.objects.get(id=report_guid)
    Model = apps.get_model(report.model)
    query_set = Model.objects.filter(pk__in=report.subset)

    meta = Model._meta
    field_names = [field.name for field in meta.fields]

    buffer = StringIO()
    writer = csv.writer(buffer)

    writer.writerow(field_names)
    for obj in query_set:
        writer.writerow([getattr(obj, field) for field in field_names])

    buffer.seek(0)

    report.contents = buffer.read()
    report.ready = True
    report.save()

    mail = EmailMultiAlternatives(
        subject="Report ready!",
        to=[report.owner.email],
        body="""
        Your report is ready!\n\nGo to %s to download the file.
        """
        % urljoin(settings.EXTERNAL_HOST, "admin/reporting/report/"),
    )
    mail.send(fail_silently=True)
