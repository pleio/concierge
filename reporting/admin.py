import zipfile

from django.contrib import admin, messages
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import gettext_lazy as _

from .models import Report


class ReportAdmin(admin.ModelAdmin):
    list_display = ("__str__", "model", "created_at", "ready")

    actions = ("download_reports",)

    def get_fieldsets(self, request, obj=None):
        return [
            (None, {"fields": ("id", "created_at", "ready")}),
        ]

    def has_change_permission(self, request, obj=None):
        return False

    def download_reports(self, request, queryset):
        queryset = queryset.filter(ready=True)

        if queryset.count() == 0:
            messages.warning(request, "No reports available for export")
            return HttpResponseRedirect(request.path)

        if queryset.count() > 1:
            response = HttpResponse(content_type="application/zip")
            response["Content-Disposition"] = "attachment; filename=reports.zip"

            zip_file = zipfile.ZipFile(response, "w")
            for report in queryset:
                if report.ready:
                    zip_file.writestr(report.filename, report.contents)

            zip_file.close()
            return response

        else:
            report = queryset.first()

            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = "attachment; filename=%s" % report.filename

            response.write(report.contents)
            return response

    download_reports.short_description = _("Download reports")


admin.site.register(Report, ReportAdmin)
