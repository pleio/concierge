import uuid

from django.db import models
from django.utils import timezone


class ReportManager(models.Manager):
    def create_report(self, model, queryset, owner):
        Report.objects.values_list()
        return self.create(
            model=model._meta.label,
            subset=[str(pk) for pk in queryset.values_list("pk", flat=True)],
            owner=owner,
            name="%s requested to export %s; %s records" % (owner.name, model._meta.label, queryset.count()),
        )


class Report(models.Model):
    class Meta:
        ordering = ("-created_at",)

    objects = ReportManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    subset = models.JSONField()
    created_at = models.DateTimeField(default=timezone.localtime)
    owner = models.ForeignKey("core.User", on_delete=models.CASCADE)
    contents = models.TextField()
    ready = models.BooleanField(default=False)

    @property
    def guid(self):
        return str(self.pk)

    def __str__(self):
        return self.name

    @property
    def filename(self):
        return "%s_%s.csv" % (self.model, self.created_at.strftime("%Y%m%d-%H%M%S"))
