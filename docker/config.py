import os
import sys
from urllib.parse import urljoin

from django.utils.translation import gettext_lazy as _

from core.helpers import str_to_bool

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = str_to_bool(os.getenv("DEBUG"))

EXTERNAL_HOST = os.getenv("EXTERNAL_HOST")
USE_X_FORWARDED_HOST = os.getenv("USE_X_FORWARDED_HOST", False)
USE_X_FORWARDED_PORT = os.getenv("USE_X_FORWARDED_PORT", False)

LOCAL_INSTALLED_APPS = os.getenv("LOCAL_INSTALLED_APPS").split(",") if os.getenv("LOCAL_INSTALLED_APPS") else []
LOCAL_AUTHENTICATION_BACKENDS = (
    os.getenv("LOCAL_AUTHENTICATION_BACKENDS").split(",") if os.getenv("LOCAL_AUTHENTICATION_BACKENDS") else []
)
LOCAL_AUTH_PASSWORD_VALIDATORS = (
    os.getenv("LOCAL_AUTH_PASSWORD_VALIDATORS").split(",") if os.getenv("LOCAL_AUTH_PASSWORD_VALIDATORS") else []
)

# SECURITY WARNING: set this to True when running in production
SESSION_COOKIE_SECURE = str_to_bool(os.getenv("SESSION_COOKIE_SECURE"))
SESSION_COOKIE_HTTPONLY = str_to_bool(os.getenv("SESSION_COOKIE_HTTPONLY"))
SESSION_COOKIE_SAMESITE = False

ALLOWED_HOSTS = [os.getenv("ALLOWED_HOST")]


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": os.getenv("DB_HOST"),
        "USER": os.getenv("DB_USER"),
        "PASSWORD": os.getenv("DB_PASSWORD"),
        "NAME": os.getenv("DB_NAME"),
    }
}

if "test" in sys.argv:
    DATABASES["default"] = {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }


TIME_ZONE = os.getenv("TIME_ZONE", "GMT")

LANGUAGE_CODE = os.getenv("LANGUAGE_CODE", "en")

LANGUAGES = [
    ("en", _("English")),
    ("nl", _("Dutch")),
    ("fr", _("French")),
    ("de", _("German")),
]

DEFAULT_FROM_EMAIL = os.getenv("DEFAULT_FROM_EMAIL")

EMAIL_HOST = os.getenv("EMAIL_HOST")
EMAIL_PORT = os.getenv("EMAIL_PORT")
EMAIL_HOST_USER = os.getenv("EMAIL_USER")
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_PASSWORD")
EMAIL_USE_TLS = os.getenv("EMAIL_USE_TLS")
EMAIL_USE_SSL = os.getenv("EMAIL_USE_SSL")
EMAIL_LOGO = os.getenv("EMAIL_LOGO", "images/email-logo.png")

SHOW_GOVERNMENT_BADGE = os.getenv("SHOW_GOVERNMENT_BADGE", True)

ACCOUNT_ACTIVATION_DAYS = 7

SITE_URL = os.getenv("SITE_URL", None)
# CORS_ALLOWED_ORIGINS = os.getenv("CORS_ORIGIN_WHITELIST", "").split(",")

GOOGLE_RECAPTCHA_SITE_KEY = os.getenv("GOOGLE_RECAPTCHA_SITE_KEY")
GOOGLE_RECAPTCHA_SECRET_KEY = os.getenv("GOOGLE_RECAPTCHA_SECRET_KEY")

SAML_FOLDER = os.getenv("SAML_FOLDER")

SAML2_SP = {
    "entityId": urljoin(os.getenv("EXTERNAL_HOST"), "saml/metadata/"),
    "assertionConsumerService": {
        "url": urljoin(os.getenv("EXTERNAL_HOST"), "saml/acs/"),
        "binding": "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
    },
    "singleLogoutService": {
        "url": urljoin(os.getenv("EXTERNAL_HOST"), "saml/slo/"),
        "binding": "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
    },
    "NameIDFormat": "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent",
    "x509cert": os.getenv("SAML_SP_X509CERT"),
    "privateKey": os.getenv("SAML_SP_PRIVATEKEY"),
}

# Setting CELERY_ALWAYS_EAGER to "True"  will make task being executed locally
# in the client, not by a worker.
# Always use "False" in production environment.
CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL")
BROKER_URL = os.getenv("CELERY_BROKER_URL")
CELERY_ALWAYS_EAGER = False if CELERY_BROKER_URL else True

# Stops mozilla-django-oidc classes from complaining, eventhough the settings are technically not used due to overwrites
OIDC_OP_TOKEN_ENDPOINT = ""
OIDC_OP_USER_ENDPOINT = ""
OIDC_RP_CLIENT_ID = ""
OIDC_RP_CLIENT_SECRET = ""
