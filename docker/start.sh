#!/usr/bin/env bash

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate
# Reset the sequence count since manually add id's in saml migration 0016
python manage.py sqlsequencereset saml | python manage.py dbshell

echo "Get geoip data"
/get-geoip.sh

# Start server
echo "Starting server"
uwsgi --master --processes 4 --threads 2 --http :8000 --module concierge.wsgi --buffer-size 32768 --static-map /static=/app/static --static-map /media=/app/media
