#!/usr/bin/env bash

echo "[i] Starting celery..."
celery -A concierge.celery worker -B -E -O fair -s "/tmp/celerybeat-schedule" --loglevel=info
