#!/usr/bin/env bash

# create geoip directory if neccessary
mkdir -p geoip2

# Get latest GeoIP database
if [ -e "geoip2/GeoLite2-City.mmdb" ]
then
    echo "GEOIP City database found."
else
    echo "GEOIP City database not found. Downloading..."
    wget $GEOIP_DOWNLOAD_URL/GeoLite2-City.mmdb.gz -P geoip2 \
    && gunzip geoip2/*.gz
fi

if [ -e "geoip2/GeoLite2-Country.mmdb" ]
then
    echo "GEOIP Country database found."
else
    echo "GEOIP Country database not found. Downloading..."
    wget $GEOIP_DOWNLOAD_URL/GeoLite2-Country.mmdb.gz -P geoip2 \
    && gunzip geoip2/*.gz
fi
