# Stage 1 - Compile needed python dependencies
FROM python:3.11-slim
RUN apt-get update -y
RUN apt-get install --no-install-recommends -y \
    git \
    build-essential \
    python3-dev \
    pkg-config \
    libxml2-dev \
    libxmlsec1-dev \
    libxmlsec1-openssl

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install --prefix=/install -r requirements.txt
ARG additional_packages
RUN test -z "$additional_packages" || echo $additional_packages | tr ',' ' ' | xargs pip3 install && :

# Stage 2 - Build docker image suitable for execution and deployment
FROM python:3.11-slim

# Workaround for error with postgresql-client package
RUN mkdir -p /usr/share/man/man1/ /usr/share/man/man3/ /usr/share/man/man7/

RUN apt-get update -y
RUN apt-get install --no-install-recommends -y \
    libxmlsec1-openssl \
    ca-certificates \
    libpq5 \
    mime-support \
    wget \
    gettext \
    postgresql-client \
    && rm -rf /var/lib/apt/lists/*

COPY --from=0 /install /usr/local

# App assets
COPY . /app
COPY ./docker/config.py /app/concierge/config.py
COPY ./docker/wait-for-it.sh ./docker/start.sh ./docker/start-dev.sh ./docker/start-background.sh ./docker/get-geoip.sh /
RUN chmod +x /wait-for-it.sh /start.sh /start-dev.sh /get-geoip.sh

# Create media and static folders
RUN mkdir /app/media /app/static && chown www-data:www-data /app/media /app/static

RUN chown -R www-data:www-data /app/geoip2

WORKDIR /app
EXPOSE 8000
USER www-data
CMD ["/wait-for-it.sh", "${DB_HOST}:5432", "--", "/start.sh"]
