from unittest import mock

from django.contrib.messages.storage.base import BaseStorage, Message
from django.core import mail
from django.test.client import RequestFactory

from core.models import User
from core.tests import TestCase
from saml.models import SamlIdentityProvider
from saml.views import check_externalid, connect


class TestMessagesBackend(BaseStorage):
    def __init__(self, request, *args, **kwargs):
        self._loaded_data = []
        super(TestMessagesBackend, self).__init__(request, *args, **kwargs)

    def add(self, level, message, extra_tags=""):
        self._loaded_data.append(Message(level, message, extra_tags=extra_tags))


class SamlTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.user = User.objects.create_user(name="John", email="john@user.com", password="GkCyKt6iWJVi")
        self.superuser = User.objects.create_superuser(name="John", email="john@superuser.com", password="LZwHZucJj9JD")

        self.idp = SamlIdentityProvider.objects.create(
            shortname="idp1",
            displayname="idp organisation",
            entityId="http://localhost:8480/simplesaml/saml2/idp/metadata.php",
        )

        self.factory = RequestFactory()

    @mock.patch("logging.Logger.warning")
    def test_check_externalid(self, mocked_logger):
        """
        Saml user who is logging in first time does not have a connected
        concierge user.
        """
        request = self.factory.get("/")
        request._messages = TestMessagesBackend(request)
        request.session = {}
        request.session["idp"] = "idp1"
        request.session["samlUserdata"] = {
            "uid": ["1"],
            "emailaddress": ["johnsaml@user.com"],
        }

        extid = check_externalid(request, shortname=self.idp.shortname, externalid="johnsaml@user.com")

        # No externalid exists atm.
        self.assertIs(extid, None)

    @mock.patch("logging.Logger.warning")
    def test_connect_new_samluser(self, mocked_logger):
        """
        Saml user who is logging in first time does not have a connected
        concierge user A concierge user will be created based on saml user data
        and connected with saml user
        """
        request = self.factory.get("/")
        request._messages = TestMessagesBackend(request)
        request.session = {}
        request.session["idp"] = "idp1"
        request.session["externalid"] = "johnsaml@user.com"
        request.session["samlUserdata"] = {
            "uid": ["1"],
            "emailaddress": ["johnsaml@user.com"],
        }

        result = check_externalid(request, shortname=self.idp.shortname, externalid="johnsaml@user.com")
        # No externalid exsists atm.
        self.assertIs(result, None)

        # Do not connect with an existing user.
        extid = connect(request, user_email=None)
        # An externalid exists atm.
        self.assertEqual(extid.externalid, "johnsaml@user.com")
        # Externalid exists with the new user.
        self.assertEqual(extid.userid, User.objects.get(email="johnsaml@user.com"))

    @mock.patch("logging.Logger.warning")
    def test_connect_new_samluser_with_existing_user(self, mocked_logger):
        """
        Saml user who is logging in first time does not have a connected
        concierge user An existing concierge user will be connected with saml
        user.
        """
        request = self.factory.get("/")
        request._messages = TestMessagesBackend(request)
        request.session = {}
        request.session["externalid"] = "johnsaml@user.com"
        request.session["idp"] = "idp1"
        request.session["samlUserdata"] = {
            "uid": ["1"],
            "emailaddress": ["johnsaml@user.com"],
        }

        result = check_externalid(request, shortname=self.idp.shortname, externalid="johnsaml@user.com")
        # No externalid exists atm.
        self.assertIs(result, None)

        # Connect with an existing user.
        extid = connect(request, user_email="john@user.com")
        # An externalid exists atm.
        self.assertEqual(extid.externalid, "johnsaml@user.com")
        # Externalid exists with the new user.
        self.assertEqual(extid.userid, User.objects.get(email="john@user.com"))

        # No email to request a new password has been sent
        self.assertEqual(len(mail.outbox), 0)
        # Empty the test outbox
        mail.outbox = []
