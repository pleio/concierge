from django.contrib import admin

from saml.models import IdpEmailDomain, OidcIdentityProvider, SamlIdentityProvider

# Register your models here.


class SamlIdentityProviderAdmin(admin.ModelAdmin):
    fields = [
        "shortname",
        "displayname",
        "metadata_url",
        "metadata_filename",
        "perform_slo",
        "connect_automatically",
    ]


admin.site.register(SamlIdentityProvider, SamlIdentityProviderAdmin)


class IdpEmailDomainAdmin(admin.ModelAdmin):
    list_display = ("email_domain", "identityprovider")


admin.site.register(IdpEmailDomain, IdpEmailDomainAdmin)


class OidcIdentityProviderAdmin(admin.ModelAdmin):
    fields = [
        "shortname",
        "displayname",
        "client_id",
        "client_secret",
        "token_endpoint",
        "authorize_endpoint",
        "jwks_endpoint",
        "end_session_endpoint",
        "userinfo_endpoint",
        "sign_algo",
    ]


admin.site.register(OidcIdentityProvider, OidcIdentityProviderAdmin)
