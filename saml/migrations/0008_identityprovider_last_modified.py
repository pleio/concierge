# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-26 09:39
from __future__ import unicode_literals

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("saml", "0007_auto_20180323_1508"),
    ]

    operations = [
        migrations.AddField(
            model_name="identityprovider",
            name="last_modified",
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
