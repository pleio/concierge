# Generated by Django 3.2.18 on 2023-04-02 10:03

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("saml", "0016_saml_and_oidc_idp"),
    ]

    operations = [
        migrations.AddField(
            model_name="oidcidentityprovider",
            name="userinfo_endpoint",
            field=models.URLField(blank=True, max_length=256),
        ),
    ]
