import logging
import re

from constance import config as constance_config
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import redirect, render
from django.template import RequestContext
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.constants import OneLogin_Saml2_Constants as saml2_constants
from onelogin.saml2.settings import OneLogin_Saml2_Settings

from core.helpers import quote_once
from core.lib import Reverse
from core.models import User
from saml.models import ExternalId, SamlIdentityProvider

logger = logging.getLogger(__name__)

try:
    base_configuration = settings.SAML2_SP_BASE_CONFIGURATION
except AttributeError:
    base_configuration = {
        "strict": True,
        "debug": settings.DEBUG,
        "sp": settings.SAML2_SP,
        "security": {
            "requestedAuthnContext": False,
            "authnRequestsSigned": not settings.DEBUG,
            "logoutRequestSigned": not settings.DEBUG,
            "logoutResponseSigned": not settings.DEBUG,
            "signatureAlgorithm": ("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"),
        },
    }


def init_saml_auth(req, idp_shortname=None):
    try:
        idp = SamlIdentityProvider.objects.get(shortname=idp_shortname)
    except SamlIdentityProvider.DoesNotExist:
        logger.error("saml.views.init_saml_auth,  no IdentityProvider found")
        idp = None

    if idp:
        configuration = {
            "idp": idp.get_saml_configuration(),
        }

        auth = OneLogin_Saml2_Auth(req, {**base_configuration, **configuration})

        return auth

    return None


def prepare_django_request(request, idp_shortname=None):
    """
    If server is behind proxys or balancers use the HTTP_X_FORWARDED fields
    Check USE_X_FORWARDED_HOST and USE_X_FORWARDED_PORT in config.py
    """
    req = {
        "https": "on" if request.is_secure() else "off",
        "http_host": request.get_host(),
        "script_name": request.META["PATH_INFO"],
        "get_data": request.GET.copy(),
        "post_data": request.POST.copy(),
    }
    if idp_shortname:
        request.session["idp"] = idp_shortname
    elif "idp" in req["get_data"]:
        request.session["idp"] = req["get_data"]["idp"]

    return req


def sso(request, idp_shortname):
    req = prepare_django_request(request, idp_shortname=idp_shortname)
    auth = init_saml_auth(req, idp_shortname=idp_shortname)
    saml_next = request.GET.get("next")
    if saml_next:
        request.session["saml_next"] = quote_once(saml_next)

    try:
        return HttpResponseRedirect(auth.login())
    except AttributeError:
        messages.error(
            request,
            _("Configuration error: IdentityProvider not found. Please contact %(support)s.")
            % {"support": constance_config.EMAIL_SUPPORT},
            extra_tags="SAML_login_error",
        )
        request.session.pop("samlConnect", None)
        reverse_url = Reverse(request=request, expected_parameters=["username", "next"])
        return redirect(reverse_url("login"))


@csrf_exempt
def acs(request):
    # Log the incoming request details
    logger.debug("ACS endpoint called with request: %s", request)

    saml_next = request.session.pop("saml_next", None)
    req = prepare_django_request(request)
    idp_shortname = request.session.get("idp")

    if not idp_shortname:
        logger.error("IDP shortname not found in session, redirecting to LOGIN_REDIRECT_URL")
        request.session.pop("samlConnect", None)
        return redirect(settings.LOGIN_REDIRECT_URL)

    auth = init_saml_auth(req, idp_shortname=idp_shortname)

    try:
        auth.process_response()
        errors = auth.get_errors()
        if errors:
            logger.error("SAML processing errors: %s", errors)
            error_reason = auth.get_last_error_reason()
            logger.error("SAML error reason: %s", error_reason)
    except Exception as e:
        logger.exception("Exception processing SAML response: %s", e)
        messages.error(
            request,
            _("Configuration error: IdentityProvider not found. Please contact support."),
            extra_tags="SAML_login_error",
        )
        return redirect(settings.LOGIN_REDIRECT_URL)

    if not errors:
        attributes = auth.get_attributes()
        name_id = auth.get_nameid()
        nameid_format = auth.get_nameid_format()
        session_index = auth.get_session_index()

        request.session["samlUserdata"] = attributes
        request.session["samlNameId"] = name_id
        request.session["samlSessionIndex"] = session_index

        # Log the SAML attributes, NameID, and session index
        logger.debug("SAML attributes: %s", attributes)
        logger.debug("SAML NameID: %s", name_id)
        logger.debug("SAML SessionIndex: %s", session_index)

        if settings.DEBUG:
            ln = len(attributes.items())

            response_xml = auth.get_last_response_xml(pretty_print_if_possible=False)
            pos_1 = response_xml.find("<saml:NameID")
            pos_2 = response_xml.find("</saml:NameID") + len("</saml:NameID>")
            logger.error("saml.views.acs, NameID: %s", response_xml[pos_1:pos_2])
            logger.error("saml.views.acs, name_id: %s", name_id)
            logger.error("saml.views.acs, nameid_format: %s", nameid_format)
            logger.error("saml.views.acs, number of attributes: %s", ln)
            for attr in attributes.items():
                logger.error("saml.views.acs, attribute: %s", attr)
        if nameid_format in [
            saml2_constants.NAMEID_PERSISTENT,
            saml2_constants.NAMEID_UNSPECIFIED,
        ]:
            externalid = name_id
        else:
            logger.error("Unexpected NameID format: %s", nameid_format)
            messages.error(
                request,
                _("Configuration error: no persistent nameid in samlUserdata found. Please contact support."),
                extra_tags="nameid_not_provided",
            )
            return redirect(settings.LOGIN_URL)

        if isinstance(externalid, list):
            externalid = externalid[0]

        request.session["externalid"] = externalid

        if not externalid:
            logger.error("saml.views.acs, no identification in samlUserdata found")
            messages.error(
                request,
                _("Identification not provided in saml request. Please contact %(support)s.")
                % {"support": constance_config.EMAIL_SUPPORT},
                extra_tags="idp_not_provided",
            )
            return redirect(settings.LOGIN_URL)

        extid = check_externalid(request, shortname=idp_shortname, externalid=externalid)

        if not extid:
            saml_user_data = request.session.get("samlUserdata")
            email = None
            if saml_user_data:
                email = get_email_from_saml_attributes(saml_user_data)
                if isinstance(email, (list,)):
                    email = email[0]
            if email:
                # check to see wether there exists an account for
                # this email address
                try:
                    idp = SamlIdentityProvider.objects.get(shortname=idp_shortname)
                except SamlIdentityProvider.DoesNotExist:
                    logger.warning("saml.views.acs, no IdentityProvider found")
                    messages.error(
                        request,
                        _("IdentityProvider not found. Please contact %(support)s.")
                        % {"support": constance_config.EMAIL_SUPPORT},
                        extra_tags="identityprovider_doesn't_exists",
                    )
                    return redirect(settings.LOGIN_URL)
                if idp.connect_automatically:
                    try:
                        user = User.objects.get(email__iexact=email)
                        connect_and_login(request, user_email=email)
                    except User.DoesNotExist:
                        connect_and_login(request, user_email=None)
                    extid = check_externalid(request, shortname=idp_shortname, externalid=externalid)
                else:
                    try:
                        user = User.objects.get(email__iexact=email)
                        request.session["saml_context"] = "show_connect"
                    except User.DoesNotExist:
                        request.session["saml_context"] = "show_connectanduser"
                    return redirect(settings.LOGIN_URL)
            else:
                logger.error("saml.views.acs, no emailaddress in samlUserdata found")
                messages.error(
                    request,
                    _("Email address not provided in saml request. Please contact %(support)s.")
                    % {"support": constance_config.EMAIL_SUPPORT},
                    extra_tags="email_not_provided",
                )
                request.session.pop("samlConnect", None)

                return redirect(settings.LOGIN_URL)

        user = User.objects.get(pk=extid.userid.pk)
        logger.debug("User object retrieved: %s", user)

        request.session["via_sso"] = True
        from core.class_views import BaseLoginView

        pl = BaseLoginView()
        return pl.post_login_process(request, user, extid=extid, saml_next=saml_next, via_saml=True)
    else:
        error_reason_code = auth.get_last_error_reason()
        logger.error(
            _("saml.views.acs, errors found: {errors!r} reason: {reason!r}").format(
                errors=errors, reason=error_reason_code
            )
        )
        messages.error(
            request,
            _("SAML login error found. Please contact %(support)s. Error: %(error)s. Reason: %(reason)s.")
            % {
                "support": constance_config.EMAIL_SUPPORT,
                "error": errors,
                "reason": error_reason_code,
            },
            extra_tags="SAML_login_error",
        )
        request.session.pop("samlConnect", None)

    logger.debug("Redirecting to LOGIN_URL: %s", settings.LOGIN_URL)
    return redirect(settings.LOGIN_URL)


@csrf_exempt
def slo(request):
    req = prepare_django_request(request)
    if request.session.pop("slo", None):
        req["get_data"]["slo"] = "slo"

    idp_shortname = request.session.get("idp")

    if not idp_shortname:
        logger.error("saml.views.slo,  no IdentityProvider found")
        return redirect(settings.LOGIN_REDIRECT_URL)

    auth = init_saml_auth(req, idp_shortname=idp_shortname)

    if "slo" in req["get_data"]:
        return HttpResponseRedirect(
            auth.logout(
                return_to=settings.LOGOUT_REDIRECT_URL,
                name_id=request.session.get("samlNameId"),
                session_index=request.session.get("samlSessionIndex"),
            )
        )

    else:
        return redirect(settings.LOGOUT_REDIRECT_URL)


@csrf_exempt
def saml(request):
    # do nothing, because a user never logges in at our place
    return redirect(settings.LOGOUT_REDIRECT_URL)


def attrs(request):
    paint_logout = False
    attributes = False

    if "samlUserdata" in request.session:
        paint_logout = True
        if len(request.session["samlUserdata"]) > 0:
            attributes = request.session["samlUserdata"].items()

    return render(
        request,
        "saml_attrs.html",
        context=RequestContext(request, {"paint_logout": paint_logout, "attributes": attributes}).flatten(),
    )


def metadata(request):
    """
    req = prepare_django_request(request)
    auth = init_saml_auth(req)
    saml_settings = auth.get_settings()
    OneLogin is expecting the sp settings in settings.json in
    settings.SAML_FOLDER
    """
    saml_settings = OneLogin_Saml2_Settings(settings=base_configuration, custom_base_path=None, sp_validation_only=True)
    metadata = saml_settings.get_sp_metadata()
    errors = saml_settings.validate_metadata(metadata)

    if len(errors) == 0:
        resp = HttpResponse(content=metadata, content_type="text/xml")
    else:
        resp = HttpResponseServerError(content=", ".join(errors))
    return resp


def check_externalid(request, **kwargs):
    shortname = kwargs.get("shortname")
    externalid = kwargs.get("externalid")

    try:
        idp = SamlIdentityProvider.objects.get(shortname=shortname)
    except SamlIdentityProvider.DoesNotExist:
        logger.warning("saml.views.check_externalid, no IdentityProvider found")
        messages.error(
            request,
            _("IdentityProvider not found. Please contact %(support)s.") % {"support": constance_config.EMAIL_SUPPORT},
            extra_tags="identityprovider_doesn't_exists",
        )
        return None

    request.session["samlConnect"] = True

    try:
        extid = ExternalId.objects.get(identityproviderid=idp, externalid__iexact=externalid)
    except ExternalId.DoesNotExist:
        logger.warning("saml.views.check_externalid, no ExternalId found")
        extid = None

    return extid


def connect_and_login(request, user_email=None):
    extid = connect(request, user_email=user_email)
    if not extid:
        return redirect(settings.LOGIN_URL)

    try:
        user = User.objects.get(pk=extid.userid.pk)
    except User.DoesNotExist:
        logger.error("saml.views.connect_and_login, can't connect, no existing user found")
        messages.error(
            request,
            _("Can't connect account for it doesn't seem to exists"),
            extra_tags="user_doesn't_exists",
        )
        return None

    from core.class_views import BaseLoginView

    pl = BaseLoginView()
    return pl.post_login_process(request, user, extid=extid, via_saml=True)


def connect(request, user_email=None):
    """
    if user_email = None
        a new  concierge account for this user has to be created
        saml account connects with this new concierge account
    else saml account connects to existing concierge account
    """
    idp_shortname = request.session.get("idp")
    if not idp_shortname:
        return None

    saml_user_data = request.session.get("samlUserdata")
    if saml_user_data:
        email = get_email_from_saml_attributes(saml_user_data)
        if isinstance(email, (list,)):
            email = email[0]

    if saml_user_data:
        name = saml_user_data.get("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name") or saml_user_data.get(
            "name"
        )
        if name and len(name) > 0:
            if isinstance(name, (list,)):
                name = name[0]

        if not name or len(name) == 0:
            givenname = saml_user_data.get("givenname")
            if givenname and len(givenname) > 0:
                if isinstance(givenname, (list,)):
                    givenname = givenname[0]
            if not givenname:
                givenname = ""
            surname = saml_user_data.get("surname")
            if surname and len(surname) > 0:
                if isinstance(surname, (list,)):
                    surname = surname[0]
            if not surname:
                surname = ""
            name = (givenname + " " + surname).strip()

        if not name or len(name) == 0:
            left_part = re.findall(r"^(.*)@", email)
            name = left_part[0]

    try:
        idp = SamlIdentityProvider.objects.get(shortname=idp_shortname)
    except SamlIdentityProvider.DoesNotExist:
        logger.error("saml.views.connect, no IdentityProvider found")
        messages.error(
            request,
            _("IdentityProvider not found. Please contact %(support)s.") % {"support": constance_config.EMAIL_SUPPORT},
            extra_tags="identityprovider_doesn't_exists",
        )
        return None

    if user_email:
        # connect to existing concierge account
        try:
            user = User.objects.get(email__iexact=user_email)
        except User.DoesNotExist:
            logger.error("saml.views.connect, can't connect, no existing user found")
            messages.error(
                request,
                _("Can't connect account for it doesn't seem to exists"),
                extra_tags="user_doesn't_exists",
            )
            return None
    else:
        try:
            if not email:
                logger.error("saml.views.connect, no emailaddress in samlUserdata found")
                messages.error(
                    request,
                    _("Email address not provided in saml request. Please contact %(support)s.")
                    % {"support": constance_config.EMAIL_SUPPORT},
                    extra_tags="email_not_provided",
                )
                return None
            user = User.objects.create_user(email=email, name=name, accepted_terms=True)
            user.is_active = True
            user.save()
        except IntegrityError:
            logger.error("saml.views.connect, can't create, user already exists")
            messages.error(
                request,
                _("An account for this email address already exists"),
                extra_tags="user_exists",
            )
            return None

    externalid = request.session.get("externalid")

    if not externalid:
        logger.error("saml.views.connect, no identification in samlUserdata found")
        messages.error(
            request,
            _("Identification not provided in saml request. Please contact %(support)s.")
            % {"support": constance_config.EMAIL_SUPPORT},
            extra_tags="email_not_provided",
        )
        return redirect(settings.LOGIN_REDIRECT_URL)

    try:
        extid = ExternalId.objects.get(identityproviderid=idp, externalid__iexact=externalid)
    except ExternalId.DoesNotExist:
        extid = ExternalId.objects.create(identityproviderid=idp, externalid=externalid, userid=user)

    return extid


def get_email_from_saml_attributes(saml_userdata):
    email = (
        saml_userdata.get("http://schemas.xmlsoap.org/ws/2005/05/" + "identity/claims/emailaddress")
        or saml_userdata.get("emailaddress")
        or saml_userdata.get("urn:oid:0.9.2342.19200300.100.1.3")
    )
    return email


@login_required
def show_connections(request):
    connections = ExternalId.objects.filter(userid=request.user)
    return render(request, "show_connections.html", {"connections": connections})


def delete_connection(request, pk=None):
    if pk:
        try:
            ExternalId.objects.get(pk=pk).delete()
        except Exception:
            pass

    return redirect("saml_connections")


@csrf_exempt
def info(request):
    idp_shortname = request.session.get("idp")

    try:
        idp = SamlIdentityProvider.objects.get(shortname=idp_shortname)
    except SamlIdentityProvider.DoesNotExist:
        idp = None

    if idp:
        identity_provider = idp.displayname
    else:
        identity_provider = None

    externalid = request.session.get("externalid")
    try:
        extid = ExternalId.objects.get(identityproviderid=idp, externalid__iexact=externalid)
    except ExternalId.DoesNotExist:
        extid = None

    if extid:
        saml_user_data = request.session.get("samlUserdata")
        for claim, value in saml_user_data.items():
            if isinstance(value, (list,)):
                if len(value) > 0:
                    value = value[0]
                else:
                    value = ""
            saml_user_data[claim] = value
        name_id = request.session.get("samlNameId")
    else:
        saml_user_data = {}
        name_id = None

    return render(
        request,
        "show_attributes.html",
        {
            "identity_provider": identity_provider,
            "externalid": externalid,
            "name_id": name_id,
            "samlUserdata": saml_user_data,
        },
    )
