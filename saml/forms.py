from django import forms
from django.contrib.auth import password_validation

from core import messages

from .models import ExternalId


class SetPasswordForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

    new_password1 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={"autofocus": True}),
        error_messages={"required": messages.PASSWORD_REQUIRED},
    )

    new_password2 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput,
        error_messages={"required": messages.PASSWORD_REQUIRED},
    )

    def clean_new_password2(self):
        new_password1 = self.cleaned_data.get("new_password1")
        new_password2 = self.cleaned_data.get("new_password2")

        if new_password1 and new_password2 and new_password1 != new_password2:
            raise forms.ValidationError(
                messages.PASSWORD_MISMATCH,
                code="password_mismatch",
            )

        password_validation.validate_password(self.cleaned_data.get("new_password2"))
        return new_password2


class ShowConnectionsForm(forms.ModelForm):
    class Meta:
        model = ExternalId
        fields = ("identityproviderid", "externalid")
