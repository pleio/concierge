# Concierge
This is the microservice used for handling user registration, login and SAML2
SSO. It is based on the [Django project](https://www.djangoproject.com/).


## Setup development (through docker-compose)

Copy docker/config.py to concierge/config.py

Add `account.pleio.local` to your internal network IP in your hosts file.

Example (/etc/hosts):

```
192.168.2.128   account.pleio.local
```

Make sure [Docker](https://www.docker.com/) is installed. Then run the
following commands within the repository:

    docker-compose up

This will spin up a PostgreSQL container and a Django web container. Then
create a superuser account using:

    docker-compose exec web python manage.py createsuperuser

Now login with your new (superuser) account on http://account.pleio.local:8005

### Changing the theme
By default docker-compose will use the generic theme.  To specify a different
theme, run the following commands:

    docker-compose build --build-arg additional_packages={theme_name} web
    docker-compose run --rm -e LOCAL_INSTALLED_APPS={theme_name} -p 8000:8000 web
> Where {theme_name} is an available theme package, like [these](#themes).

### Connect backend2 to account.pleio.local

Add OpenID Connect Provider client:

Name: backend2
Client Type: Confidetial
Respose types: code (Authorization Code Flow)
Redirect URIs: http://*.pleio.local:8000/oidc/callback/
JWT Algorithm: HS256

Use the client ID and SECRET for the backend2 `.env` file:

```
OIDC_RP_CLIENT_ID=<CLIENT_ID>
OIDC_RP_CLIENT_SECRET=<CLIENT_SECRET>

OIDC_OP_AUTHORIZATION_ENDPOINT=http://account.pleio.local:8005/openid/authorize/
OIDC_OP_TOKEN_ENDPOINT=http://account.pleio.local:8005/openid/token/
OIDC_OP_USER_ENDPOINT=http://account.pleio.local:8005/openid/userinfo/
OIDC_OP_LOGOUT_ENDPOINT=http://account.pleio.local:8005/openid/logout/
```

## Setup development (manually)

### Python
To setup the development environment, make sure Python3 is installed on your
development machine. On OSX install libxml dependencies with:

```bash
brew install libxml2 libxmlsec1 pkg-config
```

Then run the following commands:

```bash
mkvirtualenv concierge --python=/usr/bin/python3
pip install -r requirements.txt
```

Create your configuration file

```bash
sudo cp concierge/config.example.py concierge/config.py
```

and set all configuration variables in `concierge/config.py` accordingly.

### Themes
You could use the `generic` theme or follow any instructions provided by the
custom themes to complete the installation.

- [Pleio](https://github.com/Pleio/concierge-theme-pleio) `concierge_theme_pleio`
- [Government of Canada](https://github.com/gctools-outilsgc/concierge-theme-gc) `concierge_theme_gc`

#### Build assets

```
# Move to the generic themes directory
cd themes/generic

npm install
npm run build

# Move back to the root directory
cd ../..

docker-compose exec web python manage.py collectstatic --noinput

```

### Database provisioning
Create a database using

    python manage.py migrate

Now create a superuser account using:

    python manage.py createsuperuser

And fill in the details for your superuser.

### Running
Start the Django server by using:

    python manage.py runserver

Now login with your new (superuser) account on http://127.0.0.1:8000

## Deploy to Kubernetes

    kubectl create namespace concierge
    kubectl create -f ./docs/kubernetes/deployment.yaml

## Generate new translations
We use the standard
[i18n toolset of Django](https://docs.djangoproject.com/en/1.10/topics/i18n/).
To add new translations to the source files use:

    python manage.py makemessages -a

To compile the translations use:

    python manage.py compilemessages

On OSX first make sure gettext (> 0.14) is installed and linked using:

    brew install gettext
    brew link --force gettext

## Run tests
To run the accompanied test suite use the following command:

    python manage.py test

Or when running from docker-compose:

    docker-compose run --rm web python manage.py test

## SAML Connection

With the `idp` service, you can test a SAML connection locally. After a succesful installation of Concierge, you are required to configure this SAML provider in the admin panel.
* Add a new identity provider at http://account.pleio.local:8005/admin/saml/samlidentityprovider/. Remember the shortname and provide http://localhost:8080/simplesaml/saml2/idp/metadata.php as the metadata url.
* Add an idp email domain at http://<host>:8005/admin/saml/idpemaildomain/ (*`<host>` should be your internal network ip, not localhost as concierge needs to reach it through the docker network*). This will let you use the SAML login when you login directly on Concierge with a certain email address.
* Alternatively you can add the shortname to a local backend2 website: in "admin panel" --> "general" fill in the shortname as `ID` under "Single sign-on" and give it a meaningful name. This will add an additional login button. The site needs to be closed (i.e. non-public) for the login to be visible.

There are 2 default accounts configured in the SAML idp: `user1` and `user2`. Both have the password `password`. Customization of their attributes can be done in `saml_dev/users.php`. Their `id` attribute is currently set as the persistent NameID. This can be altered in `saml_dev/saml20-idp-hosted.php`. Both of these files are mounted as a volume in the idp service so if you change these you will need to restart the service.
```
docker-compose stop idp
docker-compose up -d idp
```
