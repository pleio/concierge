{% load sessions i18n %}{% trans "<em>unknown on unknown</em>" as unknown_on_unknown %}{% trans "<em>unknown</em>" as unknown %}

{% trans "Dear" %} {{ user.name }},

{% trans "You logged in to your account from a new location" %}

{{ user_agent|device|default_if_none:unknown_on_unknown|safe }}
{% if city and country %}
  {{ city }}, {{ country }}
{% elif city %}
  {{ city }}
{% elif country %}
  {{ country }}
{% endif %}
<br />
{% if city or country %}
  <br />
  {% trans "The location mentioned is an approximate and can differ from the actual physical location." %}<br />
{% endif %}
  <br />
{% trans "If it was you safely ignore this email. Otherwise please change your password as soon as possible." %}
