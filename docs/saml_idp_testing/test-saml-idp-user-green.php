<?php

$config = array(

    'admin' => array(
        'core:AdminPassword',
    ),

    'example-userpass' => array(
        'exampleauth:UserPass',
        'gina:gina1234' => array(
            'uid' => array('1'),
            'eduPersonAffiliation' => array('group1'),
            'name' => 'Gina Vert',
        ),
        'greg:greg1234' => array(
            'uid' => array('2'),
            'eduPersonAffiliation' => array('group2'),
            'name' => 'Greg Green',
        ),
    ),

);
