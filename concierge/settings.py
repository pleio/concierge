import os

from concierge.config import LOCAL_AUTH_PASSWORD_VALIDATORS, LOCAL_AUTHENTICATION_BACKENDS, LOCAL_INSTALLED_APPS

from .logger_config import add_log_datetime, add_log_uuid

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

CONSTANCE_BACKEND = "constance.backends.database.DatabaseBackend"

INSTALLED_APPS = LOCAL_INSTALLED_APPS + [
    "constance",
    "core",
    "emailvalidator",
    "api",
    "saml",
    "oauth2_provider",
    "rest_framework",
    "rest_framework.authtoken",
    "webpack_loader",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "user_sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_otp",
    "django_otp.plugins.otp_static",
    "django_otp.plugins.otp_totp",
    "two_factor",
    "two_factor.plugins.phonenumber",
    "oidc_provider",
    "corsheaders",
    "post_deploy",
    "reporting",
    "floodctl",
    "health_check",
    "health_check.db",
    "health_check.cache",
    "health_check.storage",
    "health_check.contrib.migrations",
    "health_check.contrib.celery_ping",
    "health_check.contrib.rabbitmq",
]

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_THROTTLE_CLASSES": (
        "rest_framework.throttling.AnonRateThrottle",
        "rest_framework.throttling.UserRateThrottle",
    ),
    "DEFAULT_THROTTLE_RATES": {"anon": "10/minute", "user": "100/minute"},
}

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "csp.middleware.CSPMiddleware",
    "core.middleware.XRealIPMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "user_sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "core.middleware.ProfileCompleteMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django_otp.middleware.OTPMiddleware",
    "core.middleware.OriginSiteMiddleware",
    "core.middleware.DeviceIdMiddleware",
    "core.middleware.HealthCheckMiddleware",
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "core.oidc.auth.MultiOIDCAuthenticationBackend",
] + LOCAL_AUTHENTICATION_BACKENDS

ROOT_URLCONF = "concierge.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
                "core.context_processors.analytics_context",
                "core.context_processors.origin_context",
            ],
        },
    },
]

WSGI_APPLICATION = "concierge.wsgi.application"

SESSION_ENGINE = "user_sessions.backends.db"

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

MINIMAL_PASSWORD_LENGTH = 12

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation." + "UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation." + "MinimumLengthValidator",
        "OPTIONS": {
            "min_length": MINIMAL_PASSWORD_LENGTH,
        },
    },
    {
        "NAME": "django.contrib.auth.password_validation." + "CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation." + "NumericPasswordValidator",
    },
] + LOCAL_AUTH_PASSWORD_VALIDATORS

AUTH_USER_MODEL = "core.User"

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LOCALE_PATHS = (os.path.join(BASE_DIR, "locale"),)

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")

GEOIP_PATH = os.path.join(BASE_DIR, "geoip2/")

LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/dashboard/"
LOGOUT_REDIRECT_URL = "/logout/"

LOGIN_EMAIL_EXPIRE_MINUTES = 20
PASSWORD_RESET_EMAIL_EXPIRE_MINUTES = 20
REGISTRATION_EXPIRE_MINUTES = 24 * 60
NEW_PROFILE_EXPIRE_MINUTES = 7 * 24 * 60

OIDC_USERINFO = "concierge.oidc_provider_settings.userinfo"
OIDC_EXTRA_SCOPE_CLAIMS = "concierge.oidc_provider_settings.CustomScopeClaims"
# we have to send claims during /token and not /userinfo since we can then identify the user's session
OIDC_IDTOKEN_INCLUDE_CLAIMS = True

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "add_log_uuid": {
            "()": "django.utils.log.CallbackFilter",
            "callback": add_log_uuid,
        },
        "add_log_datetime": {
            "()": "django.utils.log.CallbackFilter",
            "callback": add_log_datetime,
        },
    },
    "formatters": {
        "verbose": {"format": "%(levelname)s %(asctime)s %(funcName)s " + "%(lineno)d %(name)s %(message)s"},
        "simple": {"format": "%(levelname)s %(log_uuid)s %(asctime)s %(message)s"},
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "filters": ["add_log_uuid"],
            "formatter": "simple",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "INFO"),
        },
    },
}

CSRF_FAILURE_VIEW = "core.views.csrf_failure"
CSRF_USE_SESSIONS = True
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

SAML_IDP_BINDING = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
SAML_IDP_NAMESPACE = {
    "md": "urn:oasis:names:tc:SAML:2.0:metadata",
    "ds": "http://www.w3.org/2000/09/xmldsig#",
}

CONSTANCE_ADDITIONAL_FIELDS = {"image_field": ["django.forms.ImageField", {}]}
# SITE_LOGO is a string so .svg images can be used. Valid input is a
# <dir>/<filename> relative to STATIC e.g. "images/logo.svg"
# Google reCAPTCHA Will be present on login page when from that IP adress more
# than RECAPTCHA_NUMBER_INVALID_LOGINS during the last
# RECAPTCHA_MINUTES_THRESHOLD have occurred.
# Request an API key at https://developers.google.com/recaptcha/ for reCAPTCHA
# validation.
CONSTANCE_CONFIG = {
    "SITE_TITLE": ("Pleio", "The title of the site", str),
    "SITE_LOGO": ("", "The filename of the logo", str),
    "SITE_FAVICON": ("", "The filename of the icon", "image_field"),
    "EMAIL_SUPPORT": (
        "support@pleio.nl",
        "The email address of the support departement",
        str,
    ),
    "SITE_SUPPORT": ("", "The url of the support site", str),
    "SITE_SUPPORT_NAME": ("", "The name of the support site", str),
    "SITE_TERMS": ("", "The url to the general terms", str),
    "EMAIL_DOMAIN": ("pleio.nl", "Domain name used for email message-id", str),
    "DEFAULT_SHOW_DESTINATION": (
        False,
        "Show default destination location and background?",
        bool,
    ),
    "DEFAULT_DESTINATION_NAME": (
        "Pleio",
        "The name of the default destination site",
        str,
    ),
    "DEFAULT_DESTINATION_URL": (
        "https://www.pleio.nl",
        "The url of the default destination site",
        str,
    ),
    "DEFAULT_BACKGROUND_URL": (
        "",
        "The filename of the default background image",
        "image_field",
    ),
    "REGISTRATION_LIMITED_TO_VALID_COUNTRIES": (
        False,
        "Limit registration to valid countries only ",
        bool,
    ),
    "REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES": (
        False,
        "Allow registration in case country cannot be determined",
        bool,
    ),
    "RECAPTCHA_MINUTES_THRESHOLD": (
        30,
        "Number of minutes used for counting invalid login attempts",
        int,
    ),
    "RECAPTCHA_NUMBER_INVALID_LOGINS": (
        10,
        "Number of invalid login attempts before reCAPTCHA is served",
        int,
    ),
    "SENDING_EMAIL_AFTER_SUSPICIOUS_LOGIN": (
        False,
        "Send an email after a login occurred from a new location",
        bool,
    ),
}
CONSTANCE_CONFIG_FIELDSETS = {
    "1. General Options": (
        "SITE_TITLE",
        "SITE_LOGO",
        "SITE_FAVICON",
        "EMAIL_SUPPORT",
        "EMAIL_DOMAIN",
        "SITE_SUPPORT",
        "SITE_SUPPORT_NAME",
        "SITE_TERMS",
    ),
    "2. Registration limitation options": (
        "REGISTRATION_LIMITED_TO_VALID_COUNTRIES",
        "REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES",
    ),
    "3. Default destination options": (
        "DEFAULT_SHOW_DESTINATION",
        "DEFAULT_DESTINATION_NAME",
        "DEFAULT_DESTINATION_URL",
        "DEFAULT_BACKGROUND_URL",
    ),
    "4. Google reCAPTCHA limitation options": (
        "RECAPTCHA_MINUTES_THRESHOLD",
        "RECAPTCHA_NUMBER_INVALID_LOGINS",
    ),
    "5. Miscellaneous": ("SENDING_EMAIL_AFTER_SUSPICIOUS_LOGIN",),
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

SILENCED_SYSTEM_CHECKS = ["admin.E410"]

# Content-Security-Policy header configuration https://django-csp.readthedocs.io/en/latest/configuration.html
CSP_DEFAULT_SRC = ["'self'"]
CSP_BASE_URI = ["'none'"]
CSP_OBJECT_SRC = ["'none'"]
CSP_REPORT_ONLY = os.getenv("CSP_REPORT_ONLY") == "True"
CSP_STYLE_SRC = [
    "'self'",
    "https://fonts.googleapis.com",
    "https://fonts.gstatic.com",
]
CSP_FONT_SRC = ["'self'", "https://fonts.gstatic.com"]
CSP_SCRIPT_SRC = [
    "'self'",
    "https://www.google.com",
    "https://www.gstatic.com",
    "https://stats.pleio.nl",
]
CSP_FRAME_SRC = ["'self'", "https://www.google.com"]
CSP_FRAME_ANCESTORS = ["'self'"]
CSP_CONNECT_SRC = [
    "'self'",
    "https://www.google.com/recaptcha/",
    "https://stats.pleio.nl",
]
CSP_INCLUDE_NONCE_IN = ["default-src", "script-src"]

if os.getenv("DEBUG"):
    # Add for local development
    CSP_REPORT_ONLY = True

POST_DEPLOY_CELERY_APP = "concierge.celery.app"
POST_DEPLOY_SCHEDULER_MANAGER = "post_deploy.plugins.scheduler.celery.CeleryScheduler"

ALLOWED_AVATAR_SIZES = [24, 32, 40, 80, 600]

# Threshold that attacks a specific identity.
FLOOD_THRESHOLD = 10
# Threshold for attacks from a specific location.
FLOOD_IP_THRESHOLD = 100
# Expire minutes for one record.
FLOOD_EXPIRE = 60

# Piwik/Matamo settings.
ANALYTICS_URL = os.getenv("ANALYTICS_URL", default=False)
ANALYTICS_SITE_ID = os.getenv("ANALYTICS_SITE_ID", default=False)

# Breaking change in https://github.com/jazzband/django-oauth-toolkit
PKCE_REQUIRED = False

AUTOTRANSLATE_TRANSLATOR_SERVICE = "core.services.translate_service.DeeplTranslatorService"
SECURE_HSTS_SECONDS = 3600
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True


# Django health check
# https://github.com/revsys/django-health-check
BROKER_URL = os.getenv("CELERY_BROKER_URL")
HEALTHCHECK_IP = os.getenv("HEALTHCHECK_IP")
HEALTH_CHECK = {
    "SUBSETS": {
        "basic": [
            # Subset requires at least one plugin, this is the fastest one
            "Cache backend: default"
        ],
        "extended": [
            "Cache backend: default",
            "CeleryHealthCheckCelery",
            "DatabaseBackend",
            "DefaultFileStorageHealthCheck",
            "MigrationsHealthCheck",
            "RabbitMQHealthCheck",
        ],
    },
}

from .config import *  # noqa
