"""concierge URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import URLPattern, URLResolver, include, path, re_path
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.i18n import JavaScriptCatalog
from oauth2_provider import views as oauth2_views
from oidc_provider.views import ProviderInfoView
from two_factor.urls import urlpatterns as tf_urls

from api import views as api_views
from core import views
from core.class_views import (
    IDPLoginView,
    Login2faView,
    PasswordLoginView,
    PasswordResetConfirmView,
    PasswordResetView,
    PrimaryLoginView,
    SamlConnectLoginView,
    SessionDeleteOtherView,
    SessionDeleteView,
)
from core.oidc import views as oidc_views
from saml import views as saml_views


class DecoratedURLPattern(URLPattern):
    def resolve(self, *args, **kwargs):
        result = super(DecoratedURLPattern, self).resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


class DecoratedRegexURLResolver(URLResolver):
    def resolve(self, *args, **kwargs):
        result = super(DecoratedRegexURLResolver, self).resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


def decorated_includes(func, includes):
    """
    Include URLconf from module but apply the specified decorator to each.
    """
    urlconf_module, app_name, namespace = includes
    urlconf_urls = urlconf_module.urlpatterns

    for item in urlconf_urls:
        if isinstance(item, URLPattern):
            item.__class__ = DecoratedURLPattern
            item._decorate_with = func

        elif isinstance(item, URLResolver):
            item.__class__ = DecoratedRegexURLResolver
            item._decorate_with = func

    return urlconf_module, app_name, namespace


legacy_urls = [path("action/logout", views.logout, name="logout_legacy")]

urls = [
    path("register/", views.register, name="register"),
    path("register/complete/", views.register_complete, name="register_complete"),
    re_path(
        r"^register/activate/(?P<activation_token>[-:\w]+)/$",
        views.register_activate,
        name="register_activate",
    ),
    path("register/complete_profile/", views.complete_profile, name="complete_profile"),
    re_path(r"^register/(?P<email>[0-9A-Za-z_\-@+.]+)$", views.register, name="register"),
    re_path(
        r"^resend-new-email-activation/$",
        views.send_new_email_activation,
        name="send_new_email_activation",
    ),
    re_path(
        r"^change-email/activate/(?P<activation_token>[-:\w]+)/$",
        views.change_email_activate,
        name="change_email_activate",
    ),
    re_path(r"^pages/(?P<page_name>[\w-]+)/$", views.pages, name="pages"),
    re_path(
        r"^pages/(?P<page_name>[\w-]+)/(?P<language_code>[\w-]+)/$",
        views.pages,
        name="pages",
    ),
    re_path(
        r"^securitypages/(?P<page_action>[\w-]+)/$",
        views.security_pages,
        name="security_pages",
    ),
    path("securitypages/", views.security_pages, name="security_pages"),
    path("password_reset/", view=PasswordResetView.as_view(), name="password_reset"),
    path("password_reset/sent/", views.password_reset_sent, name="password_reset_sent"),
    re_path(
        r"^password_reset/(?P<pending_user_id>[-:\w]+)/$",
        view=PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path("login/", PrimaryLoginView.as_view(), name="login"),
    path("login/pwd/", PasswordLoginView.as_view(), name="login_pwd"),
    path("login/idp/", IDPLoginView.as_view(), name="login_idp"),
    path("login/sent/", views.email_sent, name="login_email_sent"),
    re_path(
        r"^login/email/(?P<pending_user_id>.+)/$",
        views.login_with_email,
        name="login_email",
    ),
    path("login2fa/", Login2faView.as_view(), name="login2fa"),
    re_path(r"^login2fa/(?P<login_step>[\w-]+)/$", Login2faView.as_view(), name="login2fa"),
    re_path(r"^avatar/(?P<user_id>\d+)/(?P<filename>.+)$", api_views.avatar, name="avatar"),
    path("logout/", views.logout, name="logout"),
    re_path(
        r"^profile/delete/(?P<code>.+)/$",
        views.confirm_delete_account,
        name="confirm_delete_account",
    ),
    path("profile/delete/", views.initiate_delete_account, name="delete_account"),
    path("profile/", views.profile, name="profile"),
    path("dashboard/", views.show_dashboard, name="dashboard"),
    path(
        "account/sessions/other/delete/ ",
        view=SessionDeleteOtherView.as_view(),
        name="session_delete_other",
    ),
    re_path(
        r"^account/sessions/(?P<pk>\w+)/delete/$",
        view=SessionDeleteView.as_view(),
        name="session_delete",
    ),
    path(
        "oauth/v2/authorize",
        oauth2_views.AuthorizationView.as_view(),
        name="authorize",
    ),
    path("oauth/v2/token", oauth2_views.TokenView.as_view(), name="token"),
    path(
        "oauth/v2/revoke_token",
        oauth2_views.RevokeTokenView.as_view(),
        name="revoke-token",
    ),
    path("api/users/me", api_views.me, name="me"),
    path("api/users/me/change_avatar", api_views.change_avatar, name="change_avatar"),
    path("api/users/me/remove_avatar", api_views.remove_avatar, name="remove_avatar"),
    path("api/users/me/change_name", api_views.change_name, name="change_name"),
    path("api/users/me/change_email", api_views.change_email, name="change_email"),
    path(
        "api/users/me/change_password",
        api_views.change_password,
        name="change_password",
    ),
    path("api/users/deleted", api_views.deleted_accounts, name="deleted_accounts"),
    path(
        "api/users/get_or_create",
        api_views.get_or_create_user,
        name="get_or_create_user",
    ),
    re_path(
        r"^api/users/fetch_profile/(?P<user_id>\d+)$",
        api_views.fetch_profile,
        name="fetch_profile",
    ),
    re_path(
        r"^api/users/fetch_profile_mail/(?P<email>.+)$",
        api_views.fetch_profile_mail,
        name="fetch_profile_mail",
    ),
    re_path(
        r"^api/users/fetch_avatar/(?P<email>.+)$",
        api_views.fetch_avatar,
        name="fetch_avatar",
    ),
    re_path(
        r"^api/users/register_origin_site/(?P<user_id>\d+)$",
        api_views.register_origin_site,
        name="register_origin_site",
    ),
    re_path(
        r"^api/users/update_origin_site$",
        api_views.update_origin_site,
        name="update_origin_site",
    ),
    path("ht/", include("health_check.urls")),
    path("admin/", admin.site.urls),
    path("", views.home, name="home"),
]

tf_urls = [path("", include(tf_urls))]

us_urls = [path("", include("user_sessions.urls", "user_sessions"))]


oidc_urls = [
    re_path(r"^openid/logout/?$", views.oidc_logout, name="oidc_logout"),
    re_path(
        r"^openid/authorize/?$",
        oidc_views.PleioAuthorizeView.as_view(),
        name="authorize",
    ),
    re_path(
        r"^openid/token/?$",
        csrf_exempt(oidc_views.PleioTokenView.as_view()),
        name="token",
    ),
    re_path(
        r"^openid/(?P<provider>[\w-]+)/authenticate/$",
        oidc_views.PleioAuthenticateView.as_view(),
        name="oidc_authentication_init",
    ),
    re_path(
        r"^openid/(?P<provider>[\w-]+)/callback/$",
        oidc_views.PleioCallbackView.as_view(),
        name="oidc_authentication_callback",
    ),
    path(
        "openid/",
        decorated_includes(
            xframe_options_exempt,
            include("oidc_provider.urls", namespace="oidc_provider"),
        ),
    ),
    re_path(
        r"^\.well-known/openid-configuration/$",
        ProviderInfoView.as_view(),
        name="provider_info",
    ),
]

django_urls = [
    path("jsi18n", JavaScriptCatalog.as_view(), name="javascript-catalog"),
    path("i18n/", include("django.conf.urls.i18n")),
]

saml_urls = [
    re_path(r"^saml/sso/(?P<idp_shortname>[\w-]+)/$", saml_views.sso, name="saml_sso"),
    path("saml/acs/", saml_views.acs, name="saml_acs"),
    path("saml/info/", saml_views.info, name="saml_info"),
    path("saml/slo/", saml_views.slo, name="saml_slo"),
    path("saml/attrs/", saml_views.attrs, name="saml-attrs"),
    path("saml/metadata/", saml_views.metadata, name="saml-metadata"),
    path("saml/connect/", saml_views.connect_and_login, name="saml-connect"),
    path("saml/connect_new/", SamlConnectLoginView.as_view(), name="saml-connect-new"),
    path("saml/connections/", saml_views.show_connections, name="saml_connections"),
    re_path(
        r"^saml/connection_delete/(?P<pk>\w+)/$",
        saml_views.delete_connection,
        name="saml_connection_delete",
    ),
]

urlpatterns = legacy_urls + urls + tf_urls + us_urls + oidc_urls + django_urls + saml_urls

if settings.DEBUG:
    from core import error_views

    debug_urls = [
        path("error500/", error_views.error_500, name="error500"),
    ]
    urlpatterns += debug_urls
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler500 = "core.error_views.error_500"
