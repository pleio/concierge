from django.utils.translation import gettext_lazy as _

FIRST_LAST_NAME_INVALID = _("The first- and lastname contains invalid characters.")
NAME_REQUIRED = _("Please Enter your first- and your lastname.")
PASSWORD_MISMATCH = _("Please enter exactly the same password twice.")
PASSWORD_REQUIRED = _("Please enter your password.")
EMAIL_REQUIRED = _("Please enter your email address.")
EMAIL_INVALID = _("Please enter a valid email address.")
ACCEPTED_TERMS_REQUIRED = _("Please accept the terms in order to proceed.")
DUPLICATE_EMAIL = _("This email is already registered.")
DUPLICATE_USERNAME = _("This username is already registered.")
NAME_INVALID = _("The name contains invalid characters.")
PASSWORD_INVALID = _("The password is invalid.")
CAPTCHA_MISMATCH = _("captcha_mismatch")
