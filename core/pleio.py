from hashlib import md5
from urllib.parse import urlencode

import requests
from django.conf import settings
from django.utils import timezone

from core.models import OriginSite


class PleioClient:
    logger = None

    def __init__(self, site: OriginSite):
        self.site = site

    def add_checksum(self, data=None):
        data = data or {}

        # Do not overwrite timestamp key in data for testing purposes.
        data.setdefault("timestamp", int(timezone.now().timestamp()))
        checksum = md5(self.site.origin_site_api_token.encode())
        for k, v in sorted(data.items(), key=lambda x: [str(y).lower() for y in x]):
            checksum.update(str(k).encode())
            checksum.update(str(v).encode())
        data["checksum"] = checksum.hexdigest()[:12]

        if settings.DEBUG and self.logger:
            self.logger.error("Content-Type: application/x-www-form-urlencoded")
            self.logger.error(urlencode(data))

        return data

    def post(self, path, data=None):
        if settings.DEBUG and self.logger:
            self.logger.error("Post data to %s", self.site.url(path))
        return requests.post(self.site.url(path), data=self.add_checksum(data))

    def get(self, path):
        if settings.DEBUG and self.logger:
            self.logger.error("Get data from %s", self.site.url(path))
        return requests.get(self.site.url(path), data=self.add_checksum())
