from django.contrib import admin
from django.views.generic.detail import DetailView

from core.models import DisplayNameHistory


class DisplayNameChangeInspectView(DetailView):
    permission_required = "view_displaynamehistory"
    template_name = "admin/core/display_name_change/inspect.html"
    model = DisplayNameHistory

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            **admin.site.each_context(self.request),
            "opts": self.model._meta,
            "display_name_history": DisplayNameHistory.objects.filter(user=self.object.user).order_by("-created_at"),
        }
