from django.conf import settings

from core.models import OriginSite


def analytics_context(request):
    return {
        "ANALYTICS_ENABLED": settings.ANALYTICS_URL and settings.ANALYTICS_SITE_ID,
        "ANALYTICS_URL": settings.ANALYTICS_URL,
        "ANALYTICS_SITE_ID": settings.ANALYTICS_SITE_ID,
    }


def origin_context(request):
    origin = None
    if request.COOKIES.get("origin_site_id"):
        try:
            origin = OriginSite.objects.filter(id=int(request.COOKIES.get("origin_site_id"))).first()
        except Exception:
            pass

    return {
        "ORIGIN": origin,
    }
