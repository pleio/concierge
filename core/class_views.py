import logging
from urllib.parse import parse_qs, unquote, urlparse

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.views import RedirectURLMixin
from django.core.exceptions import ValidationError
from django.shortcuts import redirect, render, resolve_url
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.utils.http import url_has_allowed_host_and_scheme
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django_otp.plugins.otp_static.models import StaticToken
from two_factor.utils import default_device
from two_factor.views.core import BackupTokensView as BackupTokensViewBase
from two_factor.views.profile import ProfileView as ProfileViewBase
from user_sessions.views import SessionDeleteOtherView as SessionDeleteOtherViewBase
from user_sessions.views import SessionDeleteView as SessionDeleteViewBase
from user_sessions.views import SessionListView as SessionListViewBase

from core.forms import (
    AuthenticationForm,
    AuthenticationTokenForm,
    BackupTokenForm,
    UsernameInputForm,
)
from core.helpers import constance_value, quote_once, set_origin_cookie
from core.lib import Reverse, force_same_site_url, terminate_local_sessions
from core.mailers import (
    send_activation_token,
    send_login_by_mail_message,
    send_password_reset_link,
)
from core.models import EventLog, PendingUser, User
from floodctl.models import FloodLog
from floodctl.utils import (
    FloodProtectionMixin,
    RedirectOnOverflowMixin,
    flood_protection,
)
from saml import views as saml_views
from saml.models import IdpEmailDomain

logger = logging.getLogger(__name__)


class BaseLoginView(RedirectOnOverflowMixin, RedirectURLMixin, TemplateView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.reverse = None

    def dispatch(self, request, *args, **kwargs):
        self.reverse = Reverse(request, expected_parameters=["username", "next"])
        return super().dispatch(request, *args, **kwargs)

    def get_redirect_url(self, saml_next=None):
        if saml_next:
            redirect_to = unquote(saml_next)
        else:
            try:
                if self.request.method == "GET":
                    redirect_to = unquote(self.request.GET.get("next"))
                else:
                    redirect_to = unquote(self.request.POST.get("next"))
            except Exception:
                redirect_to = None

        url_is_safe = url_has_allowed_host_and_scheme(
            url=redirect_to,
            allowed_hosts=self.get_success_url_allowed_hosts(),
            require_https=self.request.is_secure(),
        )

        if url_is_safe:
            return redirect_to
        else:
            return resolve_url(settings.LOGIN_REDIRECT_URL)

    def post_login_process(self, request, user, extid=None, saml_next=None, via_saml=False):
        self.request = request
        if self.check_is_banned(request, user):
            return redirect(settings.LOGIN_URL)

        if not self.check_is_active(request, user):
            return redirect(settings.LOGIN_URL)

        request.session["username"] = user.email
        device = default_device(user)
        if not device or via_saml:
            return self.post_login_user(request, user, extid, saml_next)
        else:
            request.session["login_step"] = "token"
            next = request.GET.get("next") or request.POST.get("next") or saml_next
            if next:
                return redirect("/login2fa/token/?next=" + quote_once(next))
            else:
                return redirect("/login2fa/token")

    def post_login_user(self, request, user, extid=None, saml_next=None):
        auth_login(request, user, backend="django.contrib.auth.backends.ModelBackend")
        EventLog.add_event(request, EventLog.USER_LOGGED_IN)
        user.check_users_previous_logins(request)

        if request.session.pop("samlConnect", None):
            request.session["samlLogin"] = True
            if not extid:
                saml_views.connect(request, user.email)

        next = request.GET.get("next") or request.POST.get("next") or saml_next
        if next:
            return redirect(self.get_redirect_url(next))

        return redirect(self.get_redirect_url())

    def check_is_active(self, request, user=None):
        if not user:
            username = request.POST.get("username")
            try:
                user = User.objects.get(email__iexact=username)
            except User.DoesNotExist:
                return False

        if not user.is_active:
            try:
                pending_user = PendingUser.objects.get(user=user, type=PendingUser.PENDING_REGISTRATION)
                pending_user.assert_not_due()
            except PendingUser.DoesNotExist:
                pending_user = PendingUser.objects.create(
                    user=user,
                    type=PendingUser.PENDING_REGISTRATION,
                    next=force_same_site_url(request, request.GET.get("next")),
                )
                send_activation_token(user, pending_user)

            messages.info(request, _("This account is inactive."), extra_tags="inactive")
            return False

        return True

    def check_is_banned(self, request, user):
        if user.is_banned:
            messages.error(request, _("This account is suspended."), extra_tags="banned")
            return True

        return False

    def _get_username(self, username):
        try:
            user = User.objects.get(email__iexact=username)
            return user.email
        except User.DoesNotExist:
            pass
        return username

    def get_idp_domain(self, username):
        try:
            username = self._get_username(username)
            email_domain = username.split("@")[1]
            try:
                return IdpEmailDomain.objects.get(email_domain=email_domain)
            except IdpEmailDomain.DoesNotExist:
                pass
        except (AttributeError, IndexError):
            pass
        return None

    def is_username_idp(self, username):
        return bool(self.get_idp_domain(username))

    @staticmethod
    def handle_email_login(username, request):
        try:
            with flood_protection(request, username):
                user = User.objects.get(email=username)
                if not PendingUser.objects.filter_recent(
                    user=user, minutes=15, type=PendingUser.PENDING_LOGIN
                ).exists():
                    pending_user = PendingUser.objects.create(
                        user=user,
                        type=PendingUser.PENDING_LOGIN,
                        next=force_same_site_url(request, request.GET.get("next")),
                    )
                    send_login_by_mail_message(pending_user)
        except User.DoesNotExist:
            FloodLog.objects.add_record(request, username)
            pass


class PrimaryLoginView(BaseLoginView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = kwargs.get("form")

        saml_context = self.request.session.get("saml_context")
        if saml_context:
            context[saml_context] = True
            context["show_recaptcha"] = form.recaptcha_required()
        else:
            context["show_next"] = True
            context["show_register"] = True

        form.fields["username"].widget.attrs.update({"autofocus": "autofocus"})

        context["account_type"] = constance_value("SITE_TITLE")

        return context

    def get(self, request, *args, **kwargs):
        form = UsernameInputForm(request=request, initial={"username": request.GET.get("username")})
        next = request.GET.get("next") or ""
        set_origin_cookie(self.request)

        # is it an OpenID/SAML login?
        if next and urlparse(next).path == "/openid/authorize/":
            idp = parse_qs(next).get("idp")
            if idp and isinstance(idp, (list,)):
                idp = idp[0]
            if idp:
                return saml_views.sso(request, idp)

        # is it an OAuth2/SAML login?
        if next and urlparse(next).path == "/oauth/v2/authorize":
            logintype = parse_qs(next).get("logintype")
            if logintype and isinstance(logintype, (list,)):
                logintype = logintype[0]
            if logintype == "crawler":
                context = self.get_context_data(form=form, logintype="crawler")
                return render(request, "login/credentials.html", context)

            idp = parse_qs(next).get("idp")
            if idp and isinstance(idp, (list,)):
                idp = idp[0]
            if idp:
                return saml_views.sso(request, idp)

            method = parse_qs(next).get("method")
            if method and isinstance(method, (list,)):
                method = method[0]
            if method == "register":
                return redirect("/register")

        context = self.get_context_data(form=form)
        return render(request, "login/credentials.html", context)

    def post(self, request):
        if "button_register" in request.POST:
            return redirect(self.reverse("register"))

        form = UsernameInputForm(request=request, data=request.POST)

        if not form.is_valid():
            context = self.get_context_data(form=form)
            return render(request, "login/credentials.html", context)

        if self.is_username_idp(form.cleaned_data["username"]):
            return redirect(self.reverse("login_idp"))

        return redirect(self.reverse("login_pwd"))


class PasswordLoginView(BaseLoginView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = kwargs.get("form")

        if "username" not in self.request.GET:
            form.fields["username"].widget.attrs.update({"autofocus": "autofocus"})
        else:
            form.fields["password"].widget.attrs.update({"autofocus": "autofocus"})
        context["show_recaptcha"] = form.recaptcha_required()
        context["account_type"] = constance_value("SITE_TITLE")
        context["pwd_reset_link"] = self.reverse("password_reset")

        return context

    def get(self, request, *args, **kwargs):
        username = request.GET.get("username")
        form = AuthenticationForm(request=request, initial={"username": username})

        context = self.get_context_data(form=form)

        return render(request, "login/credentials_pwd.html", context)

    def require_idp_view(self, actual_email, expected_email):
        """
        When the visitor changed the email adres in between get and post
         to one for which an IDP is defined we move to the IDP
         authentication handling.
        """
        if actual_email == expected_email:
            return False

        actual_has_idp = self.is_username_idp(actual_email)
        if not actual_has_idp:
            return False

        return True

    def post(self, request):
        if "button_email_login" in request.POST:
            self.handle_email_login(request.POST.get("username"), request)
            return redirect(self.reverse("login_email_sent"))

        if "button_back" in request.POST:
            return redirect(self.reverse("login", post=False))

        expected_username = request.GET.get("username")
        actual_email = request.POST.get("username")
        if self.require_idp_view(actual_email=actual_email, expected_email=expected_username):
            return redirect(self.reverse("login_idp"))

        if request.POST.get("username"):
            form = AuthenticationForm(request=request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get("username")
                try:
                    user = User.objects.get(email__iexact=username)
                except User.DoesNotExist:
                    try:
                        user = User.objects.get(username__iexact=username)
                    except User.DoesNotExist:
                        raise form.ValidationError(
                            self.error_messages["invalid_login"],
                            code="invalid_login",
                            params={"username": username},
                        )
                return self.post_login_process(request, user)
            else:
                if form.errors.get("__all__") and "inactive" in form.errors.get("__all__"):
                    try:
                        user = User.objects.get(email__iexact=form.cleaned_data.get("username"))
                    except User.DoesNotExist:
                        user = None
                    if user:
                        messages.info(
                            request,
                            _("A new activation link is sent to your email address."),
                        )
                        send_activation_token(user, self.get_or_create_activation_token(user=user))
                        EventLog.add_event(request, EventLog.REGISTRATION_EMAIL_RESEND, user)
                    else:
                        EventLog.add_event(request, EventLog.USER_INACTIVE_AND_UNKNOWN)
                else:
                    EventLog.add_event(request, EventLog.INVALID_LOGIN_USERID_PASSWORD)
        else:
            form = AuthenticationForm(request=request, initial=request.POST)

        context = self.get_context_data(form=form)

        return render(request, "login/credentials_pwd.html", context)

    @staticmethod
    def get_or_create_activation_token(user):
        if token := PendingUser.objects.filter(user=user).filter_valid_pending_users().order_by("-created_at").first():
            return token
        return PendingUser.objects.create(user=user)


class IDPLoginView(BaseLoginView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = kwargs.get("form")
        username = kwargs.get("username")

        idp = self.get_idp_domain(username)

        if not idp:
            return context

        context["idp"] = idp.identityprovider.shortname
        context["show_saml"] = True
        context["show_login"] = True
        context["pwd_link"] = self.reverse("login_pwd")

        form.fields["username"].widget.attrs.update({"autofocus": "autofocus"})
        return context

    def get(self, request, *args, **kwargs):
        username = request.GET.get("username")

        if not self.is_username_idp(username):
            return redirect(self.reverse("login_pwd"))

        form = AuthenticationForm(request=request, initial={"username": username})
        context = self.get_context_data(form=form, username=username)

        return render(request, "login/credentials_idp.html", context)

    def create_idp_url(self, request):
        idp_domain = self.get_idp_domain(request.POST.get("username"))
        if not idp_domain:
            return self.reverse("login")

        return self.reverse("saml_sso", args=[idp_domain.identityprovider.shortname])

    def post(self, request):
        if "button_email_login" in request.POST:
            self.handle_email_login(request.POST.get("username"), request)
            return redirect(self.reverse("login_email_sent"))

        if "button_back" in request.POST:
            return redirect(self.reverse("login", post=False))

        if "button_idp_login" in request.POST:
            return redirect(self.create_idp_url(request))

        return redirect(self.reverse("login"))


class SamlConnectLoginView(BaseLoginView):
    def get_context_data(self, **kwargs):
        kwargs["password_link"] = self.reverse("password_reset")
        return kwargs

    def get(self, request, *args, **kwargs):
        form = AuthenticationForm(request=request)

        context = self.get_context_data(form=form)

        return render(request, "login/saml_connect.html", context)

    def post(self, request):
        form = AuthenticationForm(request=request, data=request.POST)

        if form.is_valid():
            username = form.cleaned_data.get("username")
            try:
                user = User.objects.get(email__iexact=username)
            except User.DoesNotExist:
                raise form.ValidationError(
                    self.error_messages["invalid_login"],
                    code="invalid_login",
                    params={"username": username},
                )
            return self.post_login_process(request, user)

        EventLog.add_event(request, EventLog.INVALID_LOGIN_USERID_PASSWORD)
        context = self.get_context_data(form=form)
        return render(request, "login/saml_connect.html", context)


class Login2faView(BaseLoginView):
    def get_context_data(self, **kwargs):
        context = super(Login2faView, self).get_context_data(**kwargs)

        context["login_step"] = kwargs.get("login_step")

        if self.request.method == "GET":
            next = self.request.GET.get("next")
        else:
            next = self.request.POST.get("next")
        if next:
            context["next"] = quote_once(next)

        return context

    def get(self, request, *args, **kwargs):
        login_step = kwargs.get("login_step")

        if login_step == "token":
            user = User.objects.get(email=request.session.get("username"))
            form = AuthenticationTokenForm(user, request)
        elif login_step == "backup":
            user = User.objects.get(email=request.session.get("username"))
            form = BackupTokenForm(user, request)

        context = self.get_context_data(form=form, login_step=login_step)

        return render(request, "login/login2fa.html", context)

    def post(self, request, *args, **kwargs):
        login_step = kwargs.get("login_step")

        if login_step == "token":
            return self.post_token(request, *args, **kwargs)
        elif login_step == "backup":
            return self.post_backuptoken(request, *args, **kwargs)

    def post_token(self, request, *args, **kwargs):
        user = User.objects.get(email=request.session.get("username"))
        form = AuthenticationTokenForm(user, request, data=request.POST)
        if form.is_valid():
            return self.post_login_user(request, user)
        else:
            EventLog.add_event(request, EventLog.INVALID_LOGIN_TOKEN, user)

        context = self.get_context_data(form=form, login_step=request.session.get("login_step"))

        return render(request, "login/login2fa.html", context)

    def post_backuptoken(self, request, *args, **kwargs):
        user = User.objects.get(email=request.session.get("username"))
        form = BackupTokenForm(user, request, data=request.POST)
        if form.is_valid():
            return self.post_login_user(request, user)
        else:
            EventLog.add_event(request, EventLog.INVALID_LOGIN_BACKUP_TOKEN, user)

        context = self.get_context_data(form=form, login_step=request.session.get("login_step"))

        return render(request, "login/login2fa.html", context)


class ProfileView(ProfileViewBase):
    """
    View used by users for managing two-factor configuration.

    This view shows whether two-factor has been configured for the user's
    account. If two-factor is enabled, it also lists the primary verification
    method and backup verification methods.
    """

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, "get", self.http_method_not_allowed)

        self.request = request
        self.args = args
        self.kwargs = kwargs

        return handler(request, *args, **kwargs)


class SessionListView(SessionListViewBase):
    """
    View for listing a user's own sessions.

    This view shows list of a user's currently active sessions. You can
    override the template by providing your own template at
    `user_sessions/session_list.html`.
    """

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, "get", self.http_method_not_allowed)

        self.request = request
        self.args = args
        self.kwargs = kwargs

        return handler(request, *args, **kwargs)


class SessionDeleteView(SessionDeleteViewBase):
    """
    View for deleting all user's sessions but the current.

    This view allows a user to delete all other active session. For example
    log out all sessions from a computer at the local library or a friend's
    place.
    """

    def get_success_url(self):
        return str(reverse_lazy("security_pages"))


class SessionDeleteOtherView(SessionDeleteOtherViewBase):
    """
    View for deleting all user's sessions but the current.

    This view allows a user to delete all other active session. For example
    log out all sessions from a computer at the local library or a friend's
    place.
    """

    def get_success_url(self):
        return str(reverse_lazy("security_pages"))


class BackupTokensView(BackupTokensViewBase):
    """
    View for listing and generating backup tokens.

    A user can generate a number of static backup tokens. When the user loses
    its phone, these backup tokens can be used for verification. These backup
    tokens should be stored in a safe location; either in a safe or underneath
    a pillow ;-).
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        device = self.get_device()
        context["device"] = device
        context["tokens"] = device.token_set.all()

        return context

    def form_valid(self, form):
        """
        Delete existing backup codes and generate new ones.
        """
        device = self.get_device()
        device.token_set.all().delete()
        for _n in range(self.number_of_tokens):
            device.token_set.create(token=StaticToken.random_token())

        return TemplateResponse(
            self.request,
            "security_pages.html",
            {"form": form, "tokens": device.token_set.all()},
        )


class PasswordResetView(RedirectOnOverflowMixin, TemplateView):
    def get_context_data(self, **kwargs):
        kwargs["username"] = self.request.POST.get("username") or self.request.POST.get("username")
        return kwargs

    def handle_send_password_recovery_link(self, request, username):
        try:
            with flood_protection(request, username):
                user = User.objects.get(email=username)
                if not PendingUser.objects.filter_recent(
                    user=user,
                    minutes=settings.PASSWORD_RESET_EMAIL_EXPIRE_MINUTES,
                    type=PendingUser.PENDING_PWD_RESET,
                ).exists():
                    pending_user = PendingUser.objects.create(
                        user=user,
                        type=PendingUser.PENDING_PWD_RESET,
                        next=force_same_site_url(request, request.GET.get("next")),
                    )
                    send_password_reset_link(pending_user)
        except User.DoesNotExist:
            FloodLog.objects.add_record(request, username)

    def get(self, request, *args, **kwargs):
        return render(
            request,
            "password_reset.html",
            self.get_context_data(
                form=UsernameInputForm(request=request, initial={"username": request.GET.get("username")})
            ),
        )

    def post(self, request, *args, **kwargs):
        reverse_url = Reverse(request, expected_parameters=["username", "next"])
        if "button_back" in request.POST:
            return redirect(reverse_url("login", post=False))

        if "button_submit":
            self.handle_send_password_recovery_link(request, request.POST.get("username"))
            return redirect(reverse_url("password_reset_sent"))

        return render(
            request,
            "password_reset.html",
            self.get_context_data(form=UsernameInputForm(request=request, data=request.POST)),
        )


class PasswordResetConfirmView(FloodProtectionMixin, TemplateView):
    template_name = "password_reset_confirm.html"
    flood_overflow_message = _("To many invalid password-reset pages tried.")

    def __init__(self, *args, **kwargs):
        self.pending_user = None
        super().__init__(*args, **kwargs)

    def dispatch(self, request, pending_user_id, *args, **kwargs):
        try:
            self.pending_user = PendingUser.objects.get(pk=pending_user_id, type=PendingUser.PENDING_PWD_RESET)
            self.pending_user.assert_not_due()

            return super().dispatch(request, pending_user_id, *args, **kwargs)

        except (PendingUser.DoesNotExist, ValidationError, AssertionError):
            FloodLog.objects.add_record(request, target="password-reset")
            messages.error(
                request,
                _("This link is invalid or has expired. Please submit a new request to update your password."),
            )
            return redirect(reverse("login"))

    def get_context_data(self, **kwargs):
        if "form" not in kwargs:
            kwargs["form"] = SetPasswordForm(self.pending_user.user)
        return kwargs

    def post(self, request, *args, **kwargs):
        from core.tasks.terminate_sessions import terminate_subsite_sessions

        form = SetPasswordForm(self.pending_user.user, data=self.request.POST)
        if form.is_valid():
            form.save()

            pk, email = self.pending_user.user.pk, self.pending_user.user.email
            terminate_subsite_sessions.delay(pk, email)
            terminate_local_sessions(pk, email)

            EventLog.add_event(request, EventLog.PASSWORD_SET, self.pending_user.user)
            messages.success(request, _("Password has been changed successfully."))
            username, next = self.pending_user.user.email, self.pending_user.next

            # delete all pending password reset requests for this user
            PendingUser.objects.filter(user=self.pending_user.user, type=PendingUser.PENDING_PWD_RESET).delete()

            if request.user.is_authenticated and (username == request.user.email):
                update_session_auth_hash(request, form.user)
                return redirect(next or reverse("dashboard"))

            reverse_url = Reverse(request, override_parameters={"username": username, "next": next})
            return redirect(reverse_url("login_pwd"))

        return render(request, self.template_name, self.get_context_data(form=form))
