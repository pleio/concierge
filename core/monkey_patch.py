import logging

logger = logging.getLogger(__name__)


def monkey_patch():
    """
    Monkey patch for oauth2_provider 1.7.1 to accept wildcards in redirect_uri for
    """
    from django.core.exceptions import ValidationError
    from oauth2_provider import models
    from oauth2_provider.validators import RedirectURIValidator
    from urlmatch import urlmatch
    from urlmatch.urlmatch import parse_match_pattern

    logger.info("Monkey patching oauth2_provider")

    def urlmatch_allowed_urls(url, allowed_urls):
        for allowed_url in allowed_urls:
            if urlmatch(allowed_url, url):
                return True

        return False

    def url_validate(self, value):
        try:
            parse_match_pattern(value)
        except Exception:
            raise ValidationError("Invalid URL: %s" % value)

    models.redirect_to_uri_allowed = urlmatch_allowed_urls
    RedirectURIValidator.__call__ = url_validate
