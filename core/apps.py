import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class CoreConfig(AppConfig):
    name = "core"

    def ready(self):
        try:
            from core.monkey_patch import monkey_patch

            monkey_patch()
        except ImportError as e:
            logger.error("Error importing monkey_patch: %s", e)
            pass
