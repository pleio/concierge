from mixer.backend.django import mixer

from core.models import DisplayNameHistory, OriginSite, User, UserSiteRegistration
from core.tests import TestCase


class TestDisplayNameHistory(TestCase):
    def setUp(self):
        super().setUp()

        self.acting_user = mixer.blend(User, name="Origin")
        self.reference_user = mixer.blend(User, name="Destination")
        self.origin_site = mixer.blend(OriginSite)
        self.site_registration = UserSiteRegistration.objects.create(
            user=self.acting_user, origin_site=self.origin_site
        )

    def add_to_history(self):
        return DisplayNameHistory.objects.add_to_history(
            user=self.acting_user,
            name=self.reference_user.name,
            cause=DisplayNameHistory.UNKNOWN_CAUSE,
        )

    def assert_valid_display_name_history_record(self, history_record):
        self.assertEqual(history_record.user, self.acting_user)
        self.assertEqual(history_record.name, self.reference_user.name)
        self.assertEqual(history_record.cause, DisplayNameHistory.UNKNOWN_CAUSE)

    def test_exactly_the_same(self):
        expected_registration = UserSiteRegistration.objects.create(
            user=self.reference_user, origin_site=self.origin_site
        )

        history_record = self.add_to_history()
        self.assert_valid_display_name_history_record(history_record)
        self.assertEqual(1, history_record.similar_accounts.count())
        self.assertEqual(history_record.similar_accounts.first(), expected_registration)

    def test_display_alomost_the_same(self):
        history_record = self.add_to_history()
        self.assert_valid_display_name_history_record(history_record)
        self.assertEqual(0, history_record.similar_accounts.count())
