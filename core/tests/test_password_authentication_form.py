from core.factories import create_user
from core.tests import TestCase


class TestPasswordAuthenticationForm(TestCase):
    def setUp(self):
        super().setUp()

        self.user = create_user(
            username="testuser",
            email="testuser@example.com",
            password="secret",
        )

    def test_login_username(self):
        """Test logging in with a username. Should fail. Login with username is not supported."""
        response = self.client.post(
            "/login/pwd/",
            {"username": "testuser", "password": "secret"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "login/credentials_pwd.html")

    def test_login_email(self):
        """Test logging in with an email. Should pass."""
        response = self.client.post(
            "/login/pwd/",
            {"username": "testuser@example.com", "password": "secret"},
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/dashboard/")
