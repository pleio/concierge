import os
from contextlib import contextmanager
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase as TestCaseBase
from PIL import Image


def load_asset(filename):
    with open(os.path.join(os.path.dirname(__file__), "assets", filename), "rb") as fh:
        return fh.read(), filename


class TestCase(TestCaseBase):
    def setUp(self):
        super().setUp()
        self._settings = {}

        patch("warnings.warn").start()
        patch("logging.Logger.warning").start()
        patch("logging.Logger.error").start()

    def tearDown(self):
        patch.stopall()
        for setting, value in self._settings.items():
            setattr(settings, setting, value)

        super().tearDown()

    def override_settings(self, **kwargs):
        for setting, value in kwargs.items():
            if setting not in self._settings:
                self._settings[setting] = getattr(settings, setting)
            setattr(settings, setting, value)

    @contextmanager
    def assert_not_raises_exception(self, msg=None):
        try:
            yield
        except Exception:
            if msg:
                self.fail(msg)
            else:
                raise

    def assert_dict_contains_subset(self, dictionary: dict, subset: dict, msg=None):
        self.assertEqual(dictionary, dictionary | subset, msg=msg)


class FileTestCase(TestCase):
    def cleanup_file(self, filename):
        try:
            os.unlink(os.path.join(settings.MEDIA_ROOT, filename))
        except Exception:
            pass

    def assert_exists(self, filename):
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        assert os.path.exists(full_path), "Unexpectedly did not find %s" % filename

    def assert_not_exists(self, filename):
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        assert not os.path.exists(full_path), "Unexpectedly found %s" % filename


class ImageTestCase(FileTestCase):
    def cleanup_file(self, filename):
        try:
            os.unlink(os.path.join(settings.MEDIA_ROOT, filename))
        except Exception:
            pass

    def assert_exif(self, filename, msg=None):
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        image = Image.open(full_path)
        assert image.getexif(), msg or "Unexpectedly not found exif data at file %s" % filename

    def assert_no_exif(self, filename, msg=None):
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        image = Image.open(full_path)
        assert not image.getexif(), msg or "Unexpectedly found exif data at file %s" % filename

    def assert_image_size_max(self, filename, expected_wh):
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        image = Image.open(full_path)
        width, height = expected_wh
        if width:
            assert image.width <= width, f"Width unexpectedly bigger then {width}: {image.width}"
        if height:
            assert image.height <= height, f"Heigth unexpectedly bigger then {height}: {image.height}"

    def assert_image_size_min(self, filename, expected_wh):
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        image = Image.open(full_path)
        width, height = expected_wh
        if width:
            assert image.width >= width, f"Width unexpectedly less then {width}: {image.width}"
        if height:
            assert image.height >= height, f"Height unexpectedly less then {height}: {image.height}"
