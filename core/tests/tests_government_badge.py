from core.helpers import is_government
from core.tests import TestCase


class IsGovernmentTestCase(TestCase):
    """
    Run  to determine 'government' group membership
    """

    fixtures = ["automatic-test-data/government-domain.json"]

    def test_valid(self):
        """
        Test email addresses that should be valid
        """
        self.assertIs(is_government("anyone@government.com"), True)
        self.assertIs(is_government("anyone@council.com"), True)
        self.assertIs(is_government("anyone@subdomain.council.com"), True)
        self.assertIs(is_government("anyone@sub.domain.council.com"), True)

    def test_invalid(self):
        """
        Test email addresses that should be valid
        """
        self.assertIs(is_government("anyone@subdomain.government.com"), False)
        self.assertIs(is_government("anyone@sub.domain.government.com"), False)
        self.assertIs(is_government("anyone@company.com"), False)
        self.assertIs(is_government("anyone@school.edu"), False)
