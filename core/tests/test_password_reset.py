from unittest import mock

from core.factories import create_user
from core.tests import TestCase


class TestPasswordReset(TestCase):
    def setUp(self):
        super().setUp()

        self.user = create_user(
            username="testuser",
            email="testuser@example.com",
            password="secret",
        )

    @mock.patch("core.class_views.send_password_reset_link")
    def test_request_password_reset_mail(self, send_password_reset_link):
        """Test logging in with a username. Should fail. Login with username is not supported."""
        response = self.client.post(
            "/password_reset/",
            {"username": "testuser@example.com"},
            follow=True,
        )

        self.assertTrue(send_password_reset_link.called)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "password_reset_sent.html")

    @mock.patch("core.class_views.send_password_reset_link")
    def test_request_password_reset_mail_limit(self, send_password_reset_link):
        """Test logging in with a username. Should fail. Login with username is not supported."""

        # post 3 times
        for _i in range(3):
            response = self.client.post(
                "/password_reset/",
                {"username": "testuser@example.com"},
                follow=True,
            )

        # assert send_password_reset_link is called 1 time
        self.assertEqual(send_password_reset_link.call_count, 1)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "password_reset_sent.html")
