from unittest import mock

from constance import config as constance_config
from django.contrib.sessions.middleware import SessionMiddleware
from django.core import mail
from django.test.client import RequestFactory

from core.models import User
from core.tests import TestCase


class PreviousLoginTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_user(name="John", email="john@user.com", password="GkCyKt6iWJVi")
        self.superuser = User.objects.create_superuser(name="John", email="john@superuser.com", password="LZwHZucJj9JD")

        self.factory = RequestFactory()

        constance_config.SENDING_EMAIL_AFTER_SUSPICIOUS_LOGIN = True

    @mock.patch("core.login_session_helpers.warnings")
    def test_send_email_suspicious_login(self, mocked_logger):
        """Test  sending an email after a suspicious login"""
        request = self.factory.get("/")
        request.user = self.user

        get_response = mock.MagicMock()
        middleware = SessionMiddleware(get_response)
        middleware.process_request(request)

        # www.ziggo.nl an existing ip address
        request.session.ip = "8.247.18.183"
        request.session.user_agent = (
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36"
            " (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.75"
            " Chrome/62.0.3202.75 Safari/537.36"
        )

        result = self.user.check_users_previous_logins(request)
        # No previous login found
        self.assertIs(result, False)
        # An email has been sent
        self.assertEqual(len(mail.outbox), 1)
        # Empty the test outbox
        mail.outbox = []

        # Now the previous login is found
        result = self.user.check_users_previous_logins(request)
        # Confirmed previous login found
        self.assertIs(result, True)
        # No email has been sent
        self.assertEqual(len(mail.outbox), 0)
        # Empty the test outbox
        mail.outbox = []
