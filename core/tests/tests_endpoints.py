from django.test.client import RequestFactory
from oidc_provider.lib.errors import RedirectUriError
from oidc_provider.models import Client

from core.oidc.endpoints import PleioAuthorizeEndpoint
from core.tests import TestCase


class EndpointsTestCase(TestCase):
    def setUp(self):
        super().setUp()

        site = "https://www.site.nl/"
        wildcard = "https://*.wildcard.nl/"

        client = Client(name="client")
        client.redirect_uris = [site, wildcard]
        client.save()

        self.client_id = client.client_id

        self.factory = RequestFactory()

    def create_authorize_request(self, redirect_uri):
        return self.factory.get(
            f"/openid/authorize?redirect_uri={redirect_uri}&client_id={self.client_id}&response_type=code"
        )

    def test_approved_redirect_uri(self):
        redirect_uri = "https://www.site.nl/"
        request = self.create_authorize_request(redirect_uri)

        PleioAuthorizeEndpoint(request).validate_params()

    def test_rejected_redirect_uri(self):
        redirect_uri = "https://attacker.com/"
        request = self.create_authorize_request(redirect_uri)

        with self.assertRaises(RedirectUriError):
            PleioAuthorizeEndpoint(request).validate_params()

    def test_wildcard_redirect_uri(self):
        redirect_uri = "https://blank.wildcard.nl/"
        request = self.create_authorize_request(redirect_uri)

        PleioAuthorizeEndpoint(request).validate_params()

    def test_hacky_wildcard_redirect_uri(self):
        redirect_uri = "https://attacker.com?.wildcard.nl/"
        request = self.create_authorize_request(redirect_uri)

        with self.assertRaises(RedirectUriError):
            PleioAuthorizeEndpoint(request).validate_params()

    def test_main_domain_redirect_uri(self):
        redirect_uri = "https://wildcard.nl/"
        request = self.create_authorize_request(redirect_uri)

        PleioAuthorizeEndpoint(request).validate_params()
