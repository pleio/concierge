import uuid
from unittest import mock

import celery.exceptions
from mixer.backend.django import mixer

from core.models import OriginSite, User, UserSiteRegistration
from core.tests import TestCase


class UserTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.API_KEY = uuid.uuid4()
        self.user = mixer.blend(User)
        self.registration = mixer.blend(UserSiteRegistration, user=self.user, origin_token=self.API_KEY)
        self.site = self.registration.origin_site

    def tearDown(self):
        OriginSite.objects.all().delete()
        super().tearDown()

    @mock.patch("core.tasks.PleioClient.post")
    def test_correct_signal_will_not_post_a_retry(self, mocked_request_post):
        mocked_request_post.return_value = type("Response", (object,), {"ok": True})

        from core.tasks import profile_updated_signal

        profile_updated_signal(self.registration.user.id)

        mocked_request_post.assert_called_with("api/profile/update/", data={"id": self.registration.user.id})

    @mock.patch("core.tasks.PleioClient.post")
    @mock.patch("core.tasks.logger.error")
    def test_failed_signal_will_post_a_retry_and_log_the_reason(self, mocked_logger, mocked_request_post):
        mocked_request_post.return_value = type("Response", (object,), {"ok": False})

        try:
            from core.tasks import profile_updated_signal

            profile_updated_signal(self.registration.user.id)
            self.fail("Unexpectedly did not retry the task")
        except celery.exceptions.Retry:
            mocked_logger.assert_called()
            mocked_request_post.assert_called()

    @mock.patch("core.tasks.PleioClient.post")
    @mock.patch("core.tasks.logger.error")
    def test_error_signal_will_post_a_retry_and_log_the_error(self, mocked_logger, mocked_request_post):
        mocked_request_post.side_effect = Exception()

        try:
            from core.tasks import profile_updated_signal

            profile_updated_signal(self.registration.user.id)
            self.fail("Unexpectedly did not retry the task")
        except celery.exceptions.Retry:
            mocked_request_post.assert_called()
            mocked_logger.assert_called()
