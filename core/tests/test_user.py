from unittest import mock

from django.contrib import auth
from django.core import mail
from django.utils import timezone
from mixer.backend.django import mixer

from core.login_session_helpers import is_name_valid
from core.mailers import send_activation_token, send_change_email_activation_token
from core.models import PendingUser, User
from core.tests import TestCase


class UserTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user: User = User.objects.create_user(name="John", email="john@user.com", password="GkCyKt6iWJVi")
        self.superuser: User = User.objects.create_superuser(
            name="John", email="john@superuser.com", password="LZwHZucJj9JD"
        )
        self.pending_user: PendingUser = mixer.blend(PendingUser, user=self.user)

    def test_generated_username(self):
        """Make sure a correct unique username is generated for each user"""
        self.assertEqual(self.user.username, "john@user.com")
        self.assertEqual(self.superuser.username, "john@superuser.com")

    def test_normal_user_is_inactive_when_created(self):
        """Make sure a (normal) user is in inactive state when being created"""
        self.assertIs(self.user.is_active, False)

    def test_superuser_is_active_when_created(self):
        """Make sure a superuser is in active state when being created"""
        self.assertIs(self.superuser.is_active, True)

    def test_login_with_email(self):
        """Make sure a user can login with email and password"""
        invalid_login = auth.authenticate(username="john@superuser.com", password="asdf")
        valid_login = auth.authenticate(username="john@superuser.com", password="LZwHZucJj9JD")
        valid_login_too = auth.authenticate(username=" John@superuser.com ".lower().strip(), password="LZwHZucJj9JD")
        self.assertEqual(invalid_login, None)
        self.assertEqual(valid_login, self.superuser)
        self.assertEqual(valid_login_too, self.superuser)

    def test_send_activation_token(self):
        """Test sending an email after user has registered"""
        send_activation_token(self.user, self.pending_user)
        # An email has been sent
        self.assertEqual(len(mail.outbox), 1)
        # Empty the test outbox
        mail.outbox = []

    def test_send_change_email_activation_token(self):
        """Test sending an email after user has changed his email address"""
        self.user.new_email = "newjohn@user.com"
        send_change_email_activation_token(self.user)
        # An email has been sent
        self.assertEqual(len(mail.outbox), 1)
        # Empty the test outbox
        mail.outbox = []

    def test_invalid_usernames(self):
        self.assertFalse(is_name_valid(""))
        self.assertFalse(is_name_valid("."))
        self.assertFalse(is_name_valid("http://test.com"))
        self.assertFalse(is_name_valid("A"))
        self.assertFalse(is_name_valid("www.evil.com"))
        self.assertFalse(is_name_valid("pleio.nl/lala/test.pdf"))
        self.assertTrue(is_name_valid("Abc"))
        self.assertTrue(is_name_valid("L.A."))
        self.assertTrue(is_name_valid("Ł.A."))
        self.assertTrue(is_name_valid("Şenay"))
        self.assertTrue(is_name_valid("John Doe"))
        self.assertFalse(is_name_valid("Www.Evil.Com"))
        self.assertFalse(is_name_valid("Evil.Co.In"))

    @mock.patch("core.tasks.ban_user.delay")
    def test_delete_user_directly(self, ban_user):
        # given.
        expected_id = self.user.id
        expected_mail = self.user.email

        # when
        self.user.delete()

        # Then
        self.assertTrue(ban_user.called)
        self.assertIn(expected_id, ban_user.call_args.args)
        self.assertIn(expected_mail, ban_user.call_args.args)

    @mock.patch("core.tasks.ban_user.delay")
    def test_delete_user_via_queryset(self, ban_user):
        # given.
        expected_id = self.user.id
        expected_mail = self.user.email

        # when
        User.objects.filter(id=expected_id).delete()

        # Then
        self.assertTrue(ban_user.called)
        self.assertIn(expected_id, ban_user.call_args.args)
        self.assertIn(expected_mail, ban_user.call_args.args)
        self.assertFalse(User.objects.filter(pk=expected_id).exists())


class TestExpiredUserTestCase(TestCase):
    def timedelta(self, **kwargs):
        return timezone.now() + timezone.timedelta(**kwargs)

    def setUp(self):
        super().setUp()

        self.registered_user: User = mixer.blend(User, is_active=True)
        self.registered_old_user: User = mixer.blend(User, is_active=True, time_created=self.timedelta(days=-61))

        self.unconfirmed_user: User = mixer.blend(User, is_active=False)
        self.unconfirmed_old_user: User = mixer.blend(User, is_active=False, time_created=self.timedelta(days=-61))

    def test_expired_users(self):
        self.assertEqual([*User.objects.filter_expired_users()], [self.unconfirmed_old_user])
