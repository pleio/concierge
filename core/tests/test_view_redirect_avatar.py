from django.conf import settings
from django.core.files.base import ContentFile
from django.urls import reverse
from mixer.backend.django import mixer

from core.avatar_processing import Avatar
from core.models import User
from core.tests import TestCase, load_asset


class TestViewRedirectAvatarTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user = mixer.blend(User, is_active=True)

    def tearDown(self):
        avatar = Avatar(self.user)
        avatar.previous_name = self.user.avatar.name
        avatar.cleanup_avatar()

        super().tearDown()

    @staticmethod
    def use_avatar(user):
        user.avatar = ContentFile(*load_asset("avatar-with-exif.jpg"))
        user.save()
        avatar = Avatar(user)
        avatar.create_thumbnails()
        return avatar

    def test_invalid_size(self):
        size = settings.ALLOWED_AVATAR_SIZES[0] + 1
        avatar = self.use_avatar(self.user)

        self.client.force_login(self.user)
        response = self.client.get(f"{reverse('avatar', args=[self.user.id, avatar.crc_filename()])}?size={size}")

        self.assertEqual(response.status_code, 404)

    def test_invalid_userid(self):
        user = mixer.blend(User)
        avatar = self.use_avatar(user)
        user_id = user.id
        user.delete()

        response = self.client.get(reverse("avatar", args=[user_id, avatar.crc_filename()]))

        self.assertEqual(response.status_code, 403)

    def test_invalid_filename(self):
        avatar = self.use_avatar(self.user)

        self.client.force_login(self.user)
        response = self.client.get(reverse("avatar", args=[self.user.id, "prefix_%s" % avatar.crc_filename()]))

        self.assertEqual(response.status_code, 200)

    def test_avatar(self):
        avatar = self.use_avatar(self.user)
        self.client.force_login(self.user)
        response = self.client.get(reverse("avatar", args=[self.user.id, avatar.crc_filename()]))
        self.assertEqual(response.status_code, 200)
        self.assertIn("Expires", response.headers)
        self.assertIn("Content-Length", response.headers)
        self.assertIn("Cache-Control", response.headers)
