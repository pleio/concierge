import base64
from unittest import mock

from mixer.backend.django import mixer

from core.models import OriginSite
from core.tests import TestCase, load_asset


class TestRefreshSiteFaviconTestCase(TestCase):
    def setUp(self):
        super().setUp()

        content, name = load_asset("favicon.png")
        self.origin_site: OriginSite = mixer.blend(OriginSite)
        self.expected_data = {
            "favicon": name,
            "favicon_data": base64.encodebytes(content).decode(),
        }

    def tearDown(self):
        OriginSite.objects.all().delete()
        super().tearDown()

    @mock.patch("core.tasks.PleioClient.get")
    @mock.patch("core.tasks.resize_favicon")
    def test_a_favicon_is_processed(self, mocked_resize_favicon, mocked_get):
        from core.tasks import refresh_site_favicon

        # Given.
        mocked_response = mock.MagicMock()
        mocked_response.json.return_value = self.expected_data
        mocked_response.status_code = 200
        mocked_get.return_value = mocked_response

        # When.
        refresh_site_favicon(self.origin_site.pk)
        self.origin_site.refresh_from_db()

        # Then.
        self.assertEqual(1, mocked_resize_favicon.call_count)
        self.assertIn("favicon.png", self.origin_site.origin_site_favicon.name)
