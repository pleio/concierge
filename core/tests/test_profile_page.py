from core.factories import create_user
from core.tests import TestCase


class TestProfilePage(TestCase):
    def setUp(self):
        super().setUp()
        self.user = create_user()

    def test_profile_page(self):
        self.client.force_login(self.user)
        response = self.client.get("/profile/")
        self.assertEqual(response.status_code, 200)
