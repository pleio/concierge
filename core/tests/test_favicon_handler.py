import faker
from django.core.files.base import ContentFile

from core.favicon import resize_favicon
from core.models import OriginSite
from core.tests import ImageTestCase, load_asset


class TestFaviconHandlerTestCase(ImageTestCase):
    MEDIA_ROOT = "/testing/media/"

    def setUp(self) -> None:
        super().setUp()
        self.site = OriginSite.objects.create(
            origin_site_url=faker.Faker().url(),
            origin_site_favicon=ContentFile(*load_asset("favicon.png")),
        )

    def tearDown(self) -> None:
        OriginSite.objects.all().delete()
        super().tearDown()

    def test_favicon_minifier(self):
        # Given:
        self.assertEqual(self.site.origin_site_favicon.size, 20413)
        self.assert_image_size_min(self.site.origin_site_favicon.path, (41, None))

        # When
        resize_favicon(self.site.origin_site_favicon.path)

        # Then
        self.site.refresh_from_db()
        self.assert_image_size_max(self.site.origin_site_favicon.path, (40, None))
        self.assertEqual(self.site.origin_site_favicon.size, 1675)
