import os

from PIL import Image


def resize_favicon(path):
    if os.path.exists(path):
        image = Image.open(path)

        if image.width <= 40:
            return

        factor = 40 / image.width
        image_width = round(image.width * factor)
        image_height = round(image.height * factor)
        image = image.resize(size=(image_width, image_height))

        with open(path, "wb") as fp:
            image.save(fp)
