from urllib.parse import urlparse

from django import template

register = template.Library()


@register.simple_tag
def when_test(url, label):
    parts = urlparse(url)
    test = parts.netloc.endswith("pleio-test.nl") or parts.netloc.endswith(".local")
    return label if test else ""
