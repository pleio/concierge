from django import template

from core.helpers import is_government

register = template.Library()


@register.simple_tag
def show_government_badge(email):
    return is_government(email)
