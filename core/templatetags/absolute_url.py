from urllib.parse import urljoin

from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def absolute_url(relative):
    return urljoin(settings.EXTERNAL_HOST, relative)
