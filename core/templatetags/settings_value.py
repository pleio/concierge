from django import template

from core.helpers import constance_value

register = template.Library()


@register.simple_tag
def settings_value(name):
    return constance_value(name)
