import logging

from django import template

register = template.Library()
logger = logging.getLogger(__name__)


@register.filter
def aria_invalid(wrapper):
    if wrapper.errors:
        wrapper.field.widget.attrs["aria-invalid"] = "true"
        wrapper.field.widget.attrs["aria-errormessage"] = f"{wrapper.auto_id}_error"
    return wrapper
