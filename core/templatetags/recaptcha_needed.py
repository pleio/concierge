from django import template

from core.models import EventLog

register = template.Library()


@register.simple_tag
def recaptcha_needed(request):
    return EventLog.recaptcha_needed(request)
