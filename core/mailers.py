from urllib.parse import urljoin

from constance import config as constance_config
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.core import signing
from django.core.mail import get_connection
from django.core.mail.message import EmailMultiAlternatives, make_msgid
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext as _

from core.helpers import constance_value
from core.lib import FromEmail
from core.login_session_helpers import get_city, get_country


def send_mail(
    subject,
    message,
    recipient_list,
    fail_silently=False,
    html_message=None,
    auth_user=None,
    auth_password=None,
    connection=None,
    from_name=None,
):
    """
    This method overrides django.core.mail.send_mail in order to
    specify a message-id for the email.

    Easy wrapper for sending a single message to a recipient list.
    All members of the recipient list will see the other recipients
    in the 'To' field.

    If auth_user is None, use the EMAIL_HOST_USER setting.
    If auth_password is None, use the EMAIL_HOST_PASSWORD setting.

    Note: The API for this method is frozen. New code wanting to extend the
    functionality should use the EmailMessage class directly.
    """
    connection = connection or get_connection(
        username=auth_user,
        password=auth_password,
        fail_silently=fail_silently,
    )
    message_id = make_msgid(domain=constance_config.EMAIL_DOMAIN)
    headers = {"Message-ID": message_id}
    if from_name:
        from_mail = FromEmail(settings.DEFAULT_FROM_EMAIL)
        from_mail.name = from_name
        headers["From"] = str(from_mail)

    mail = EmailMultiAlternatives(
        subject,
        message,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=recipient_list,
        connection=connection,
        headers=headers,
    )
    if html_message:
        mail.attach_alternative(html_message, "text/html")

    return mail.send()


def email_user(user, subject, message, **kwargs):
    email = kwargs.pop("email", user.email)
    fail_silently = kwargs.pop("fail_silently", None)
    html_message = kwargs.pop("html_message", None)

    send_mail(subject.strip(), message, [email], fail_silently, html_message, **kwargs)


def send_suspicious_login_message(user, request):
    session = request.session
    current_site = get_current_site(request)

    template_context = {
        "user": user,
        "city": get_city(session.ip),
        "country": get_country(session.ip),
        "user_agent": session.user_agent,
        "protocol": "https" if request.is_secure() else "http",
        "domain": current_site.domain,
    }

    email_user(
        user,
        render_to_string("emails/suspicious_login_subject.txt", template_context),
        render_to_string("emails/suspicious_login.txt", template_context),
        html_message=render_to_string("emails/suspicious_login.html", template_context),
        fail_silently=True,
    )


def send_change_email_activation_token(user):
    template_context = {
        "user": user,
        "activation_token": signing.dumps(obj=user.new_email),
    }

    email_user(
        user,
        render_to_string("emails/change_email_subject.txt", template_context),
        render_to_string("emails/change_email.txt", template_context),
        html_message=render_to_string("emails/change_email.html", template_context),
        fail_silently=True,
        email=user.new_email,
    )


def send_account_recovery_email(user):
    template_context = {
        "name": user.name,
        "password_reset": reverse("password_reset"),
        "site_name": "%s Account" % constance_value("SITE_TITLE"),
        "password_link": urljoin(
            settings.EXTERNAL_HOST,
            reverse("password_reset") + "?username=" + user.email,
        ),
        "email": user.email,
    }

    email_user(
        user,
        render_to_string(
            "emails/notify_already_has_account_subject.txt",
            template_context,
        ),
        render_to_string(
            "emails/notify_already_has_account.txt",
            template_context,
        ),
        html_message=render_to_string(
            "emails/notify_already_has_account.html",
            template_context,
        ),
        fail_silently=True,
        email=user.email,
    )


def send_activation_token(user, pending_user):
    site = pending_user.get_origin_site()
    template_context = {
        "user": user,
        "site_name": site.origin_site_name if site else constance_value("SITE_TITLE"),
        "activation_token": pending_user.id,
    }

    email_user(
        user,
        render_to_string("emails/register_subject.txt", template_context),
        render_to_string("emails/register.txt", template_context),
        html_message=render_to_string("emails/register.html", template_context),
        from_name=template_context["site_name"],
        fail_silently=True,
    )


def send_delete_account_confirm_code(user, confirmation_url):
    template_context = {
        "name": user.name,
        "email": user.email,
        "confirmation_url": confirmation_url,
        "site_url": settings.EXTERNAL_HOST,
    }
    email_user(
        user,
        subject=_("Confirm the removal of your account"),
        message=render_to_string("emails/confirm_delete.txt", template_context),
        html_message=render_to_string("emails/confirm_delete.html", template_context),
        fail_silently=True,
    )


def send_login_by_mail_message(pending_user):
    site = pending_user.get_origin_site()
    template_context = {
        "name": pending_user.user.name,
        "email": pending_user.user.email,
        "login_url": urljoin(settings.EXTERNAL_HOST, reverse("login_email", args=[pending_user.guid])),
        "due_date": pending_user.get_expire_time(),
        "site_url": settings.EXTERNAL_HOST,
        "site_name": site.origin_site_name if site else constance_value("SITE_TITLE"),
    }
    email_user(
        pending_user.user,
        subject=render_to_string("emails/login_mail_subject.txt", template_context),
        message=render_to_string("emails/login_mail_content.txt", template_context),
        html_message=render_to_string("emails/login_mail_content.html", template_context),
        from_name=template_context["site_name"],
        fail_silently=True,
    )


def send_password_reset_link(pending_user):
    site = pending_user.get_origin_site()
    template_context = {
        "name": pending_user.user.name,
        "reset_url": urljoin(
            settings.EXTERNAL_HOST,
            reverse("password_reset_confirm", args=[pending_user.guid]),
        ),
        "due_date": pending_user.get_expire_time(),
        "site_url": settings.EXTERNAL_HOST,
        "site_name": site.origin_site_name if site else constance_value("SITE_TITLE"),
    }
    email_user(
        pending_user.user,
        subject=render_to_string("emails/reset_password_subject.txt", template_context),
        message=render_to_string("emails/reset_password.txt", template_context),
        html_message=render_to_string("emails/reset_password.html", template_context),
        from_name=template_context["site_name"],
        fail_silently=True,
    )


def send_welcome_invited_user(pending_user, site, actor_name):
    template_context = {
        "actor_name": actor_name,
        "complement_account_link": urljoin(
            settings.EXTERNAL_HOST, reverse("register_activate", args=[pending_user.pk])
        ),
        "due_date": pending_user.get_expire_time(),
        "site_url": settings.EXTERNAL_HOST,
        "site_name": site.origin_site_name if site else constance_value("SITE_TITLE"),
    }

    email_user(
        pending_user.user,
        subject=render_to_string("emails/welcome_invited_user_subject.txt", template_context),
        message=render_to_string("emails/welcome_invited_user.txt", template_context),
        html_message=render_to_string("emails/welcome_invited_user.html", template_context),
        from_name=template_context["site_name"],
        fail_silently=True,
    )
