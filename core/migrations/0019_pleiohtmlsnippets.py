# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-19 09:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0018_auto_20180118_1441"),
    ]

    operations = [
        migrations.CreateModel(
            name="PleioHTMLSnippets",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("page_name", models.CharField(db_index=True, max_length=50)),
                ("language_code", models.CharField(max_length=10)),
                ("html_snippet", models.TextField()),
            ],
        ),
    ]
