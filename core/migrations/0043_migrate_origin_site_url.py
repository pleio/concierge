from urllib.parse import ParseResult, urlparse, urlunparse

from django.db import migrations


def forwards_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    OriginSite = apps.get_model("core", "OriginSite")
    originsites = OriginSite.objects.all()
    for site in originsites:
        url = urlparse(site.origin_site_url)
        site.origin_site_url = urlunparse(
            ParseResult(
                scheme=url.scheme,
                netloc=url.netloc,
                path="",
                params="",
                query="",
                fragment="",
            )
        )
        site.save()


def reverse_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    OriginSite = apps.get_model("core", "OriginSite")
    originsites = OriginSite.objects.all()
    for site in originsites:
        url = urlparse(site.origin_site_url)
        site.origin_site_url = urlunparse(
            ParseResult(
                scheme="https",
                netloc=url.netloc,
                path="/",
                params="",
                query="",
                fragment="",
            )
        )
        site.save()


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0042_user_is_smoothoperator"),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
