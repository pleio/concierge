import logging
import os
import uuid
from time import strftime
from urllib.parse import parse_qs, quote, unquote, urlparse

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


def constance_value(name):
    from constance import config as constance_config

    return getattr(constance_config, name, getattr(settings, name, ""))


def is_admin(email):
    from core.models import User

    try:
        return User.objects.get(email=email).is_superuser
    except User.DoesNotExist:
        return False


def is_government(email):
    from core.models import GovernmentDomain

    try:
        return GovernmentDomain.is_email_in_domain(None, email.lower()) and settings.SHOW_GOVERNMENT_BADGE
    except AttributeError:
        return False


def unique_filepath(self, filename):
    ext = filename.split(".")[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join("originsite/", filename)


def unique_avatar_large_filename(self, filename):
    ext = filename.split(".")[-1]
    filename = str("large." + ext)
    return unique_avatar_filepath(self.user, filename)


def unique_avatar_medium_filename(self, filename):
    ext = filename.split(".")[-1]
    filename = str("medium." + ext)
    return unique_avatar_filepath(self.user, filename)


def unique_avatar_small_filename(self, filename):
    ext = filename.split(".")[-1]
    filename = str("small." + ext)
    return unique_avatar_filepath(self.user, filename)


def unique_avatar_tiny_filename(self, filename):
    ext = filename.split(".")[-1]
    filename = str("tiny." + ext)
    return unique_avatar_filepath(self.user, filename)


def unique_avatar_topbar_filename(self, filename):
    ext = filename.split(".")[-1]
    filename = str("topbar." + ext)
    return unique_avatar_filepath(self.user, filename)


def unique_avatar_filepath(self, filename):
    return os.path.join("avatars/", strftime("%Y/%m/%d"), str(self.id), filename)


def str_to_bool(s):
    if s == "True":
        return True
    elif s == "False":
        return False
    elif s:
        return True
    elif not s:
        return False
    else:
        return None


def verify_captcha_response(response):
    try:
        data = {"secret": settings.GOOGLE_RECAPTCHA_SECRET_KEY, "response": response}
    except AttributeError:
        return True

    if not response:
        return False

    try:
        return requests.post("https://www.google.com/recaptcha/api/siteverify", data=data).json()["success"]
    except Exception:
        return False


def quote_once(next):
    if next:
        if next.find("&") > -1:
            next = quote(next)

    return next


def get_default_origin_site():
    try:
        from core.models import OriginSite

        return OriginSite.objects.get(origin_site_url=settings.EXTERNAL_HOST)
    except Exception:
        pass


def set_origin_cookie(request):
    if next := request.GET.get("next"):
        originsite = get_site_by_callback(next) or get_site_by_url(next)
        if originsite:
            # set originsite.id in cookie
            request.COOKIES.update(
                {
                    "origin_site_id": originsite.id,
                }
            )
        else:
            logger.warning("OriginSite not found for next=%s", next)
            request.COOKIES.update({"origin_site_id": None})


def get_site_by_callback(next_url):
    try:
        assert bool(next_url)
        assert isinstance(next_url, (str,))

        if next_url[0] == "/" and "?" not in next_url:
            next_url = unquote(next_url)

        assert "?" in next_url

        query = parse_qs(next_url.split("?", 1)[1])
        assert "redirect_uri" in query
        assert bool(query["redirect_uri"])

        return get_site_by_url(query["redirect_uri"][0])
    except (AssertionError, TypeError):
        pass
    return None


def get_site_by_url(next_url):
    try:
        url_parts = urlparse(next_url)
        url_match = f"{url_parts.scheme}://{url_parts.netloc}"

        from core.models import OriginSite

        return OriginSite.objects.filter(origin_site_url=url_match).first()
    except (AssertionError, TypeError):
        pass
    return None
