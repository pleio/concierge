from post_deploy import post_deploy_action


@post_deploy_action
def download_favicons():
    from core.models import OriginSite
    from core.tasks import refresh_site_favicon

    for site in OriginSite.objects.all():
        refresh_site_favicon.delay(site.id)


@post_deploy_action
def update_user_avatar():
    from core.models import User
    from core.tasks import refresh_user_avatar

    for user_id in User.objects.values_list("pk", flat=True):
        refresh_user_avatar.delay(user_id)
