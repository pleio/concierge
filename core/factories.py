from mixer.backend.django import mixer


def create_user(**kwargs):
    kwargs.setdefault("accepted_terms", True)
    kwargs.setdefault("is_active", True)

    user = mixer.blend("core.User", **kwargs)
    if "password" in kwargs:
        user.set_password(kwargs["password"])
        user.save()
    return user
