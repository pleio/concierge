import logging
import os

from django.core.exceptions import SuspiciousOperation
from django.urls import reverse
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.utils import absolutify

from saml.models import ExternalId, OidcIdentityProvider

from .user_factory import get_provider_user_factory

log_level = os.getenv("LOG_LEVEL", "WARNING")
logger = logging.getLogger(__name__)
logging.basicConfig(level=log_level.upper())


class MultiOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    def authenticate(self, request, **kwargs):
        """Authenticates a user based on the OIDC code flow."""

        self.request = request
        if not self.request:
            logger.debug("No request found for OIDC authentication")
            return None

        state = self.request.GET.get("state")
        code = self.request.GET.get("code")
        nonce = kwargs.pop("nonce", None)
        provider = kwargs.pop("provider", None)

        if not code or not state:
            logger.debug("No code or state found in request for OIDC authentication")
            return None

        self.init_for_provider(provider)

        redirect_uri = absolutify(
            self.request,
            reverse("oidc_authentication_callback", kwargs={"provider": provider}),
        )
        token_payload = {
            "client_id": self.OIDC_RP_CLIENT_ID,
            "client_secret": self.OIDC_RP_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": redirect_uri,
        }

        # Log the token payload at debug level
        logger.debug("Requesting token with payload: %s", token_payload)

        try:
            # Get the token
            token_info = self.get_token(token_payload)
            logger.debug("Received token info: %s", token_info)  # Includes sensitive data

            id_token = token_info.get("id_token")
            access_token = token_info.get("access_token")

            # Validate the token
            payload = self.verify_token(id_token, nonce=nonce)
            if payload:
                logger.debug("Token payload verified successfully")
                self.store_tokens(access_token, id_token)
                try:
                    user = self.get_or_create_user(access_token, id_token, payload)
                    logger.debug("User obtained or created successfully")
                    return user
                except SuspiciousOperation as exc:
                    logger.warning("Failed to get or create user: %s", exc)
                    return None
            else:
                logger.debug("Token payload verification failed")
                return None
        except Exception as e:
            logger.error("Error during authentication: %s", e)
            return None

    def get_userinfo(self, access_token, id_token, payload):
        """Return user details dictionary. Override of original method because some providers, like FNV,
        have no endpoint and provide a list of emails in a different claim."""

        if self.OIDC_OP_USER_ENDPOINT:
            userinfo = super().get_userinfo(access_token, id_token, payload)
        else:
            userinfo = payload

        if "email" in userinfo:
            return userinfo
        elif "emails" in userinfo:
            email = next(iter(payload.get("emails", [])), None)
            if not email:
                msg = f"User has no email address: {payload}"
                raise SuspiciousOperation(msg)

            userinfo["email"] = email
            return userinfo
        else:
            msg = f"User has no email address: {payload}"
            raise SuspiciousOperation(msg)

    def init_for_provider(self, provider_name):
        try:
            provider = OidcIdentityProvider.objects.get(shortname=provider_name)
        except OidcIdentityProvider.DoesNotExist:
            msg = f"Unknown provider {provider_name}"
            raise SuspiciousOperation(msg)

        self.provider = provider
        self.OIDC_OP_TOKEN_ENDPOINT = provider.token_endpoint
        self.OIDC_OP_USER_ENDPOINT = provider.userinfo_endpoint
        self.OIDC_OP_JWKS_ENDPOINT = provider.jwks_endpoint
        self.OIDC_RP_CLIENT_ID = provider.client_id
        self.OIDC_RP_CLIENT_SECRET = provider.client_secret
        self.OIDC_RP_SIGN_ALGO = provider.sign_algo
        self.OIDC_RP_IDP_SIGN_KEY = None

    def create_user(self, claims):
        """Return object for a newly created user account."""
        factory = get_provider_user_factory(self.provider)
        return factory.create_user(claims)

    def update_user(self, user, claims):
        """Override the base class method to update an existing user, given the claims."""
        factory = get_provider_user_factory(self.provider)
        return factory.update_user(user, claims)

    def filter_users_by_claims(self, claims):
        userid = claims.get("sub")
        if not userid:
            return self.UserModel.objects.none()

        external_ids = ExternalId.objects.filter(externalid__iexact=claims["sub"], identityproviderid=self.provider)
        users = [externalid.userid for externalid in external_ids]
        return users
