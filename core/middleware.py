import ipaddress
import json
import string
import urllib.parse
from urllib.parse import urlencode

from django.conf import settings
from django.http import HttpResponseForbidden
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.utils.deprecation import MiddlewareMixin

from core.lib import get_client_ip

VALID_KEY_CHARS = string.ascii_lowercase + string.digits


class DeviceIdMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        try:
            device_id = request.COOKIES["device_id"]
            if len(device_id) != 32:
                raise KeyError
        except KeyError:
            device_id = get_random_string(32, VALID_KEY_CHARS)

        max_age = 365 * 24 * 60 * 60  # one year
        response.set_cookie(
            "device_id",
            device_id,
            max_age=max_age,
            path=settings.SESSION_COOKIE_PATH,
            secure=settings.SESSION_COOKIE_SECURE or None,
            httponly=settings.SESSION_COOKIE_HTTPONLY or None,
        )

        return response


class OriginSiteMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        self.handle_origin_site_cookie(request, response, "origin_site_id")

        return response

    def handle_origin_site_cookie(self, request, response, cookie_name):
        try:
            if request.COOKIES[cookie_name] is None:
                request.COOKIES.pop[cookie_name]
            cookie_data = request.COOKIES[cookie_name]
            response.set_cookie(
                cookie_name,
                cookie_data,
                path=settings.SESSION_COOKIE_PATH,
                secure=settings.SESSION_COOKIE_SECURE or None,
                httponly=settings.SESSION_COOKIE_HTTPONLY or None,
            )
        except Exception:
            try:
                response.delete_cookie(
                    cookie_name,
                    path=settings.SESSION_COOKIE_PATH,
                )
            except Exception:
                pass

        return response


class XRealIPMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            real_ip = request.META["HTTP_X_REAL_IP"]
        except KeyError:
            pass
        else:
            request.META["REMOTE_ADDR"] = real_ip
        return self.get_response(request)


class ProfileCompleteMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    @staticmethod
    def _user_profile_complete(user):
        return not user.is_authenticated or user.accepted_terms

    @staticmethod
    def _is_excluded(path):
        exclude_patterns = [
            "/avatar",
            "/register",
            "/jsi18n",
            "/login",
            reverse("complete_profile"),
        ]

        for pattern in exclude_patterns:
            if path.startswith(pattern):
                return True

        return False

    def __call__(self, request):
        if not self._is_excluded(request.path) and not self._user_profile_complete(request.user):
            next = request.path + "?" + urlencode(request.GET)
            return redirect(reverse("complete_profile") + "?" + urllib.parse.urlencode({"next": next}))
        return self.get_response(request)


def can_access_health_check(request):
    if not settings.HEALTHCHECK_IP:
        return False
    ip_range = json.loads(settings.HEALTHCHECK_IP)
    try:
        ip_address_network = ipaddress.ip_network(get_client_ip(request))
        for ip in ip_range:
            if ip_address_network.subnet_of(ipaddress.ip_network(ip)):
                return True
    except ValueError:
        pass


class HealthCheckMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path.startswith("/ht/") and not can_access_health_check(request):
            return HttpResponseForbidden("Access Denied")

        return self.get_response(request)
