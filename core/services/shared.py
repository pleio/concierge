class BaseTranslatorService:
    """
    Defines the base methods that should be implemented
    """

    def translate_string(self, text, target_language, source_language="en"):
        """
        Returns a single translated string literal for the target language.
        """
        msg = ".translate_string() must be overridden."
        raise NotImplementedError(msg)

    def translate_strings(self, strings, target_language, source_language="en", optimized=True):
        """
        Returns a iterator containing translated strings for the target language
        in the same order as in the strings.
        :return:    if `optimized` is True returns a generator else an array
        """
        msg = ".translate_strings() must be overridden."
        raise NotImplementedError(msg)
