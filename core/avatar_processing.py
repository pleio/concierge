import os
from os.path import splitext

from django.conf import settings
from PIL import Image, ImageOps


class Avatar:
    SIZES = {
        "large": 80,
        "medium": 40,
        "small": 32,
    }

    def __init__(self, user):
        self.current_name = user.avatar.name or ""
        self.previous_name = user.stored_avatar or ""
        self.current_path = user.avatar.path if self.current_name else None

    def strip_exif(self):
        if self.current_name:
            filepath = os.path.join(settings.MEDIA_ROOT, self.current_name)
            avatar = Image.open(filepath).convert("RGB")
            if avatar.getexif():
                avatar = ImageOps.exif_transpose(avatar)
                avatar.save(filepath)

    def is_changed(self):
        return self.current_name != self.previous_name

    def create_thumbnails(self, size=None):
        try:
            filepath = os.path.join(settings.MEDIA_ROOT, self.current_name)
            avatar = Image.open(filepath).convert("RGB")
            avatar = self.as_square(avatar)

            if size:
                self.store_dimension(avatar, size, self.label(size))
            else:
                for label, size in self.SIZES.items():
                    self.store_dimension(avatar, size, label)
        except Exception:
            pass

    @staticmethod
    def as_square(image):
        if image.size[0] > image.size[1]:
            left = (image.size[0] - image.size[1]) / 2
            top = 0
            right = image.size[0] - left
            bottom = image.size[1]
        else:
            left = 0
            top = (image.size[1] - image.size[0]) / 2
            right = image.size[0]
            bottom = image.size[1] - top

        return image.crop((left, top, right, bottom))

    def thumbnail_filename(self, label, path=None):
        (filename, extension) = splitext(path or self.current_name)
        return f"{filename}-{label}{extension}"

    def thumbnail_filepath(self, label):
        return os.path.join(settings.MEDIA_ROOT, self.thumbnail_filename(label))

    def thumbnail_size_path(self, size):
        path = self.thumbnail_filepath(self.label(size))
        if not os.path.exists(path):
            self.create_thumbnails(size)
        return path

    def store_dimension(self, image, size, label):
        assert size in settings.ALLOWED_AVATAR_SIZES, "Invalid avatar size given"

        if size > image.size[0]:
            # Make a copy if image is smaller then required
            resized_avatar = image
        else:
            resized_avatar = image.resize((size, size), Image.LANCZOS)

        with open(self.thumbnail_filepath(label), "wb") as fp:
            resized_avatar.save(fp)

    def cleanup_avatar(self):
        if self.previous_name:
            avatar_path = os.path.join(settings.MEDIA_ROOT, self.previous_name)

            if os.path.exists(avatar_path):
                os.unlink(avatar_path)
            for size in settings.ALLOWED_AVATAR_SIZES:
                try:
                    label = self.label(size)
                    thumbnail = os.path.join(
                        settings.MEDIA_ROOT,
                        self.thumbnail_filename(label, path=avatar_path),
                    )
                    if os.path.exists(thumbnail):
                        os.unlink(thumbnail)
                except Exception:
                    pass

    def label(self, size):
        size_to_label = {sz: label for label, sz in self.SIZES.items()}
        if size in size_to_label:
            return size_to_label[size]
        return size

    def crc_filename(self):
        try:
            from hashlib import md5
            from mmap import ACCESS_READ, mmap

            filename, extension = os.path.splitext(self.current_path)
            with open(self.current_path) as file, mmap(file.fileno(), 0, access=ACCESS_READ) as file:
                return md5(file).hexdigest()[:12] + extension
        except Exception:
            return os.path.basename(self.current_path)
