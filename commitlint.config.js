module.exports = {
    extends: ['@commitlint/config-conventional'],
    plugins: [
        {
            rules: {
                'beheer-ticket': ({ subject, header }) => {
                    // Extract the scope (if exists) and the subject
                    const [scope, ...rest] = header.split(':');
                    const cleanedRest = rest.join(':').trim();  // Subject part after ':'

                    // Skip validation if the scope is "ci" or "ci(<something>)"
                    if (scope.startsWith('ci') || scope.startsWith('cd')) {
                        return [true];
                    }

                    // Skip validation for specific "chore" subject
                    if (
                        scope.startsWith('chore(release)') ||
                        scope.startsWith('chore(deps)') || // For Renovate commits
                        scope.startsWith('chore(tests)') // for e2e testing
                    ) {
                        return [true]; // Skip validation for these cases
                    }

                    // Validate ticket reference for other subjects
                    const ticketRegex = /beheer#\d+/;
                    const isValid = ticketRegex.test(subject);

                    return [
                        isValid,
                        'Your subject should include a ticket reference, e.g., beheer#1234.',
                    ];
                },
            },
        },
    ],
    rules: {
        'beheer-ticket': [2, 'always'],
    },
    ignores: [(message) => message.toLowerCase().startsWith('draft')],
};
